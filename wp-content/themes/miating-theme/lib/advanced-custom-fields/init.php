<?php
if( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly


/**
 * Hides the ACF Fields admin page from staging and production.
 */

function awesome_acf_hide_acf_admin() {

    // get the current site url
    $site_url = get_bloginfo( 'url' );

    // an array of protected site urls
    $protected_urls = array(
        'https://www.miating.nl'
    );

    // check if the current site url is in the protected urls array
    if ( in_array( $site_url, $protected_urls ) ) {

        // hide the acf menu item
        return false;

    } else {

        // show the acf menu item
        return true;

    }

}

add_filter('acf/settings/show_admin', 'awesome_acf_hide_acf_admin');


/**
 * Add global options page to site
 */
if( function_exists('acf_add_options_page') ) {

    $parent = acf_add_options_page(array(
        'page_title' 	=> 'Thema globale instellingen',
        'menu_title' 	=> 'Thema instellingen',
        'menu_slug' 	=> 'thema-globale-instellingen',
        'capability' 	=> 'edit_posts',
        'redirect' 	=> false
    ));

    acf_add_options_sub_page(array(
        'page_title' 	=> 'Thema Footer Instellingen',
        'menu_title'	=> 'Footer',
        'menu_slug' 	=> 'thema-footer-instellingen',
        'parent_slug'	=> 'thema-globale-instellingen',
    ));

    acf_add_options_sub_page(array(
        'page_title' 	=> 'Globale content',
        'menu_title'	=> 'Global content',
        'menu_slug' 	=> 'global-content',
        'parent_slug'	=> 'thema-globale-instellingen',
    ));

}

include_once( 'create_big_array.php' );

/**
 * Plugin Name: Disable ACF on Frontend
 * Description: Provides a performance boost if ACF frontend functions aren't being used
 * Version:     1.0
 * Author:      Bill Erickson
 * Author URI:  http://www.billerickson.net
 * License:     MIT
 * License URI: http://www.opensource.org/licenses/mit-license.php
 */

/**
 * Disable ACF on Frontend
 *
 */
function ea_disable_acf_on_frontend( $plugins ) {
    if( is_admin() )
        return $plugins;
    foreach( $plugins as $i => $plugin )
        if( 'advanced-custom-fields-pro/acf.php' == $plugin )
            unset( $plugins[$i] );
    return $plugins;
}
add_filter( 'option_active_plugins', 'ea_disable_acf_on_frontend' );

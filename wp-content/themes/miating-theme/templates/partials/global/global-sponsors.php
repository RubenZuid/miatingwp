<?php

$all_fields = get_acf_fields(get_the_ID());

$sponsors    = $all_fields['sponsors'];

$sponsors_title     = '';
$sponsors_backgr    = '';
$sponsors_repeater  = '';

if( $sponsors ):

    $sponsors_title     = $sponsors['title'];
    $sponsors_backgr    = $sponsors['background_image'];
    $sponsors_repeater  = $sponsors['sponsors']; //Repeater

endif;

?>

<section name="sponsors"
         class="mt-5 underglow"
         style="
                 background-image: url(<?php echo $sponsors_backgr['url'];  ?>);
                 background-repeat: no-repeat;
                 background-position: center;
                 background-size: cover;
                 ">

    <div class="pb-15 pt-5">

        <?php
        //Do regular title template
        hm_get_template_part( '/templates/partials/global/small-parts/title-regular', [ 'color' => 'color--white', 'content' => $sponsors_title ] ); ?>

        <div class="background--white py-4 my-8 px-4 px-sm-0">
            <div class="">
                <div class="row justify-content-around align-items-center">
                    <?php
                    if($sponsors_repeater):
                        $count = 1;
                        foreach($sponsors_repeater as $sponsor):

                            $sponsor_logo           = $sponsor['logo'];

                            ?>
                            <div class="col-6 col-sm-3 text-center py-2 py-sm-0">
                                <div class="flex flex-row justify-center">
                                    <img class="clients__logo-img d-inline-fleex lazy" data-src="<?php echo $sponsor_logo['sizes']['sponsor-logo']; ?>" />
                                </div>
                                <div class="text-grey-400 opacity-75 bg-black text-lg text-center pt-2">
                                    €<span class="italic"><?= $sponsor['price'] ?></span>
                                </div>
                            </div>

                            <?php
                            if( $count % 4 == 0):
                                echo '</div><div class="row">';
                            endif;

                            $count++;
                        endforeach;
                    endif;
                    ?>
                </div>
            </div>
        </div>

    </div>

</section>

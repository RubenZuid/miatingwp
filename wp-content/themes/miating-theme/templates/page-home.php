<?php
/**
 * Miating.
 *
 * This file adds the Page Contact page template
 *
 * Template Name: Page home
 * @package Miating
 * @author  Ruben Zuidervaart
 * @license GPL-2.0+
 */



add_action( 'genesis_meta', 'fiksogenesis_front_page_genesis_meta' );
/**
 * Adds widget support for homepage. If no widgets active, displays the default loop.
 *
 * @since 1.0.0
 */
function fiksogenesis_front_page_genesis_meta() {

	// Screen reader text.
	add_action( 'genesis_before_loop', 'fiksogenesis_print_screen_reader' );
}

/**
 * Function to output the accessible screen reader header for the content.
 *
 * @return string HTML string to output.
 *
 * @since 1.0.0
 */
function fiksogenesis_print_screen_reader() {

	echo '<h2 class="screen-reader-text">' . __( 'Main Content', 'fiksogenesis-pro' ) . '</h2>';

}


// Forces full width content layout.
add_filter( 'genesis_site_layout', '__genesis_return_full_width_content' );

remove_action( 'genesis_loop', 'genesis_do_loop' );
//remove_action( 'genesis_before_content_sidebar_wrap', 'fikso_do_page_hero', 1 );

add_action( 'genesis_before_content', 'fikso_do_page', 1 );


// Load page content
function fikso_do_page() {

    get_template_part( '/templates/partials/home/home-about-ruben', 'Home about Ruben' );

    //get_template_part( '/templates/partials/global/global-our-services', 'Our services' );

    get_template_part( '/templates/partials/global/global-clients', 'Clients' );

    get_template_part( '/templates/partials/global/global-reviews', 'Reviews' );


    ?>

<?php }

// Runs the Genesis loop.
genesis();

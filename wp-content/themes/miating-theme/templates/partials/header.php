<?php
/**
 * This file adds the header on the home page.
 *
 * @package Miating
 * @author  Ruben Zuidervaart
 * @license GPL-2.0+
 */

$all_fields         = get_acf_fields(get_the_ID());
$page_header_options = $all_fields['page_header_options'];

    //Get all the options
    $has_background         = '';
    $use_description        = '';
    $use_white_cta          = '';
    $use_primary_cta        = '';
    $use_narrow             = '';
    $is_low                 = '';

    if( $page_header_options ):

        $has_background         = $page_header_options['has_background'];
        $use_description        = $page_header_options['use_description'];
        $use_white_cta          = $page_header_options['use_white_cta'];
        $use_primary_cta        = $page_header_options['use_primary_cta'];
        $use_narrow             = $page_header_options['wide_or_narrow'];
        $is_low                 = $page_header_options['is_low'];

    endif;

$page_header_fields = $all_fields['page_header_fields'];

    //Setup new empty fields
    $title          = '';
    $background     = '';
    $description    = '';
    $white_cta      = '';
    $primary_cta    = '';

    $white_cta_title        = '';
    $white_cta_url          = '';
    $white_cta_show_icon    = '';

    $primary_cta_title  = '';
    $primary_cta_url    = '';

    //Fill the content
    if( $page_header_fields ):

        $title          = $page_header_fields['title'];
        $background     = $page_header_fields['background'];
        $description    = $page_header_fields['description'];
        $white_cta      = $page_header_fields['white_cta'];
        $primary_cta    = $page_header_fields['primary_cta'];

        if( $white_cta ):
            $white_cta_title      = $white_cta['title'];
            $white_cta_url      = $white_cta['url'];
            $white_cta_show_icon      = $white_cta['show_icon'];
        endif;

        if( $primary_cta ):
            $primary_cta_title      = $primary_cta['title'];
            $primary_cta_url      = $primary_cta['url'];
            $primary_link_to_demo = $primary_cta['link_to_demo'];
        endif;
    endif;

$is_low_class   = '';
if( $is_low ):
    $is_low_class   = 'hero-low';
endif;

$hero_title             = $title;
$paroller_header_class  = $is_low_class . ' paroller--header-hero';

?>

<section class="relative underglow <?php echo $paroller_header_class . '-container'; ?>" style="z-index: 5;" id="trackmouse_container">
    <img class="paroller paroller--header <?php echo $paroller_header_class; ?>"
         style="background: url('<?php echo $background['url']; ?>') no-repeat center;"

         data-paroller-factor="0.8"
         data-paroller-factor-md="0.5"
         data-paroller-factor-sm="0.5"
         data-paroller-factor-xs="0.4" >
    <div class="absolute color-overlay color-overlay--gradient">
        <div class="container h-100">
            <div class="row h-100">
                <div class="col-12 align-self-center text-center" id="trackmouse_inner">
                    <?php //Breadcrumbs
                    if ( !is_front_page() ) {
                        if (function_exists('yoast_breadcrumb')) {
                            yoast_breadcrumb('<p class="breadcrumb justify-content-center">', '</p>');
                        }
                    }
                    ?>

                    <div class="sitelogo__container">

                        <h1 class="<?php if(!$use_narrow){ echo'h-large'; } ?> color--white text-center mb-0 sitelogo__title"><?php echo $title; ?></h1>

                        <?php if( $use_description ) : ?>
                            <p class="text-center color--white text-uppercase mb-0 sitelogo__subtitle"><?php echo $description; ?></p>
                        <?php endif; ?>

                    </div>


                    <?php if( $use_white_cta || $use_primary_cta ) : ?>
                        <div class="row justify-content-center">
                            <?php if(!$use_white_cta): ?>
                                <?php //Do nothing ?>
                            <?php else: ?>
                                <div class="<?php if(!$use_primary_cta){ echo'col-12'; } else{ echo'col-md-7'; } ?> pb-3 pb-md-0">
                                    <a href="<?php if( is_post_type_archive( 'careers' ) ) { echo '#careerCards'; } else { echo $white_cta_url; } ?>" class="btn btn-white btn-block color--secondary font-weight-bold <?php if($white_cta_show_icon){ echo'btn-play'; } ?>"><?php echo $white_cta_title; ?></a>
                                    <?php if( is_post_type_archive('careers') ) { ?>
                                        <script type="text/javascript">
                                            jQuery(document).ready(function($) {
                                                $('a[href^="#"]').click(function () {
                                                    $('html, body').animate({
                                                        scrollTop: $('[name="' + $.attr(this, 'href').substr(1) + '"]').offset().top - 150
                                                    }, 500);

                                                    return false;
                                                });
                                            });
                                        </script>
                                    <?php } ?>
                                </div>
                            <?php endif; ?>

                            <?php if(!$use_primary_cta): ?>
                                <?php //Do nothing ?>
                            <?php else: ?>
                                <div class="<?php if(!$use_white_cta){ echo'col-12'; } else{ echo'col-md-5'; } ?>">
                                    <a <?php if( !$primary_link_to_demo){ echo 'href="' . $primary_cta_url . '"'; } ?> class="<?php if($primary_link_to_demo){ echo'AskDemo'; } ?> btn btn-secondary btn-block color--white font-weight-bold"><?php echo $primary_cta_title; ?></a>
                                </div>
                            <?php endif; ?>
                        </div>
                    <?php endif; ?>

                </div>

                <?php if( !$is_low ): ?>
                    <div class="scrolldown inview">
                        <div class="scrolldown--inner">

                        </div>
                    </div>
                <?php endif; ?>
            </div>
        </div>
    </div>
</section>
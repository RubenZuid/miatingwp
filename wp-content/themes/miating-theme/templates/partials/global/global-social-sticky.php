<?php

?>

<div class="absolute top-0 left-0 group socialsticky" name="Social sticky" style="z-index: 1000">
    <button class="bg-primary text-white rounded-full relative" onclick="jQuery('.SocialMenu').toggleClass('inactive active');">
        <svg xmlns="http://www.w3.org/2000/svg" aria-hidden="true"
             class="w-5 md:w-8 h-5 md:h-8 mx-auto" data-icon="hashtag" data-prefix="fal" viewBox="0 0 448 512"><path fill="currentColor" d="M446.4 182.1l1.4-8A12 12 0 00436 160h-98.6l20.3-113.9A12 12 0 00346 32h-8.1a12 12 0 00-11.8 9.9L304.9 160H177.4l20.3-113.9A12 12 0 00186 32h-8.1a12 12 0 00-11.8 9.9L144.9 160H42a12 12 0 00-11.8 9.9l-1.4 8A12 12 0 0040.6 192h98.6l-22.9 128H13.4a12 12 0 00-11.8 9.9l-1.4 8A12 12 0 0012 352h98.6L90.3 465.9A12 12 0 00102 480h8.1a12 12 0 0011.8-9.9L143.1 352h127.5l-20.3 113.9A12 12 0 00262 480h8.1a12 12 0 0011.8-9.9L303.1 352H406a12 12 0 0011.8-9.9l1.4-8a12 12 0 00-11.8-14.1h-98.6l22.9-128h102.9a12 12 0 0011.8-9.9zM276.4 320H148.7l22.9-128h127.5l-22.9 128z"/></svg>
    </button>
    <nav class="SocialMenu inactive bg-white px-3 py-3 mt-2 rounded-md  duration-500 origin-top transition-transform">
        <ul>
            <li class="">
                <a href="https://nl-nl.facebook.com/MX5meeting/">
                    <svg xmlns="http://www.w3.org/2000/svg" aria-hidden="true"
                         class="w-12" data-icon="facebook" data-prefix="fab" viewBox="0 0 512 512"><path fill="#3b5998" d="M504 256a248 248 0 10-286.8 245V327.7h-63V256h63v-54.6c0-62.2 37-96.5 93.7-96.5 27.2 0 55.5 4.8 55.5 4.8v61h-31.2c-30.8 0-40.5 19.1-40.5 38.8V256h68.8l-11 71.7h-57.8V501A248 248 0 00504 256z"/></svg>
                </a>
            </li>
            <li class="pt-3">
                <a href="#">
                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 551 551"
                    class="w-12"><linearGradient id="a" x1="275.5" x2="275.5" y1="4.6" y2="549.7" gradientTransform="matrix(1 0 0 -1 0 554)" gradientUnits="userSpaceOnUse"><stop offset="0" stop-color="#e09b3d"/><stop offset=".3" stop-color="#c74c4d"/><stop offset=".6" stop-color="#c21975"/><stop offset="1" stop-color="#7024c4"/></linearGradient><path fill="url(#a)" d="M386.9 0H164.2A164.3 164.3 0 000 164.2v222.7C0 477.4 73.6 551 164.2 551h222.7C477.4 551 551 477.4 551 387V164.2C551 73.6 477.4 0 387 0zm108.7 386.9c0 60-48.7 108.7-108.7 108.7H164.2c-60 0-108.8-48.7-108.8-108.7V164.2c0-60 48.7-108.8 108.8-108.8h222.7c60 0 108.7 48.7 108.7 108.8v222.7z"/><linearGradient id="b" x1="275.5" x2="275.5" y1="4.6" y2="549.7" gradientTransform="matrix(1 0 0 -1 0 554)" gradientUnits="userSpaceOnUse"><stop offset="0" stop-color="#e09b3d"/><stop offset=".3" stop-color="#c74c4d"/><stop offset=".6" stop-color="#c21975"/><stop offset="1" stop-color="#7024c4"/></linearGradient><path fill="url(#b)" d="M275.5 133C197 133 133 197 133 275.5S197 418 275.5 418 418 354.1 418 275.5 354.1 133 275.5 133zm0 229.6a87 87 0 110-174.2 87 87 0 010 174.2z"/><linearGradient id="c" x1="418.3" x2="418.3" y1="4.6" y2="549.7" gradientTransform="matrix(1 0 0 -1 0 554)" gradientUnits="userSpaceOnUse"><stop offset="0" stop-color="#e09b3d"/><stop offset=".3" stop-color="#c74c4d"/><stop offset=".6" stop-color="#c21975"/><stop offset="1" stop-color="#7024c4"/></linearGradient><circle cx="418.3" cy="134.1" r="34.1" fill="url(#c)"/></svg>
                </a>
            </li>
        </ul>
    </nav>
</div>

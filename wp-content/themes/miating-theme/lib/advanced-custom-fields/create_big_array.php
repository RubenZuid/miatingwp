<?php
/**
 * @param $post_id
 *
 * This function gets ALL the fields assigned to the post / page, and then stores them as a single big array.
 * This should reduce all the database calls to ONE! :D
 *
 */

function create_big_fucking_array_for_acf_posts_and_options( $post_id ) {

    // Do something with all values.
    $post_fields = get_fields( $post_id );

    // Check if a specific value was sent.
    if ( ! add_post_meta( $post_id, '_arr_all_acf_fields', $post_fields, true ) ) {
        update_post_meta ( $post_id, '_arr_all_acf_fields', $post_fields );
    }

    set_transient('acf_post_transient', $post_fields, 3600);



    // Now repeat for the options
    $options_fields = get_fields( 'options' );

    // Check if options value already exists.
    update_option('_arr_all_acf_options', $options_fields);

    set_transient('acf_options_transient', $post_fields, 3600);
}

add_action('acf/save_post', 'create_big_fucking_array_for_acf_posts_and_options', 15);
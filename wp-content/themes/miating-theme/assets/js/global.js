/**
 *
 * Add .js class to body tag if JavaScript is enabled
 *
 * @since 1.0.0
 *
 */
document.getElementsByTagName( "body" )[0].className += " js";

(function( $ ) {

	$( document ).ready( function() {
        $(function() {
            $('.lazy').lazy({
                effect: "fadeIn",
                effectTime: 2000,
                threshold: 50
            });

            $('.maplazy').lazy({
                threshold: 500
            });
        });

        $(window).scroll(function(){
            if($(document).scrollTop() > 200) {
                $('.site-header').addClass("scrolled").removeClass("normal");
            }
            else {
                $('.site-header').removeClass("scrolled").addClass("normal");
            }
        });

        $('.paroller').paroller();
	});

})(jQuery);


/**
 * Simplified modal functionality
 */
jQuery(document).ready(function($) {
    function CloseModal() {
        if ( $( '.modal' ).hasClass( 'show' ) ) {
            $('.modal').removeClass('show');
            $(document.body).removeClass('modal-open');
            if( $('.ContactModal').hasClass('d-none') ) {
                $('.ContactModal').removeClass('d-none');
            }
            if( $('.menu__hamburger').hasClass('open') ) {
                $('.menu__hamburger').removeClass('open');
            }
        }
        if ( $( '.NavSlidein' ).hasClass( 'show' ) ) {
            $('.NavSlidein').removeClass('show');
        }
    }

    function OpenContactDemo() {
        if ( !$( '.modal__contact' ).hasClass( 'show' ) ) {
            $('.modal__background').addClass('show');
            $('.modal__contact').addClass('show');
            $(document.body).addClass('modal-open');
        } else {
            CloseModal();
        }
    }

    function OpenMenu() {
        if ( !$( '.NavSlidein' ).hasClass( 'show' ) ) {
            $('.modal__background').addClass('show');
            $('.NavSlidein').addClass('show');
            $(document.body).addClass('modal-open');
            $('.ContactModal').addClass('d-none');
            $('.menu__hamburger').addClass('open');
        } else {
            CloseModal();
        }


    }

    //Modal backgrounds
    $( '.modal__background' ).click(CloseModal);
    $( '.modal__close' ).click(CloseModal);

    //Open modals
    $( '.ContactModal' ).click(OpenContactDemo);
    $( '.MenuBtn' ).click(OpenMenu);
});


/**
 * Check if Animation is currently in view
 */
jQuery(document).ready(function($) {
    function anim_in_view() {
        var window_height = $(window).height();
        var window_top_position = $(window).scrollTop();
        var window_bottom_position = (window_top_position + window_height);
        var $animations = $('.inview');

        if ($animations.length) {
            $.each($animations, function() {
                var $elm = $(this);
                var element_height = $elm.outerHeight();
                var element_top_position = $elm.offset().top + 50;
                var element_bottom_position = (element_top_position + element_height);

                if ((element_bottom_position >= window_top_position) &&
                    (element_top_position <= window_bottom_position)) {
                    $elm.delay( 2000 ).addClass( 'visible' );
                    $elm.addClass('visible');
                } else {
                    $elm.removeClass('visible');
                }
            });
        }
    }
    $(window).on('scroll resize', anim_in_view);
    setTimeout(anim_in_view, 1000)
});


//Adds the ReadMore functionality
jQuery(document).ready(function($) {
    if ($('body').hasClass('ContainsReadmores')) {
        $('.readmore__show').on("click",function(){ // When btn is pressed.
            $(this).parents('.hasReadmore').toggleClass('showReadmore');
            $(this).find('.readmore__openicon').toggleClass('fa-plus-circle fa-minus-circle');
        });
        $('.readmore__close').on("click",function(){ // When btn is pressed.
            $(this).parents('.hasReadmore').toggleClass('showReadmore');
            $('.readmore__show').find('.readmore__openicon').toggleClass('fa-plus-circle fa-minus-circle');
        });
    }

});


//Follow mouse on hover because it's fun
// ===========================================================
// See tutorial at :
// https://css-tricks.com/animate-a-container-on-mouse-over-using-perspective-and-transform/
// ===========================================================
(function() {
    // Init
    var container = document.getElementById("trackmouse_container"),
        inner = document.getElementById("trackmouse_inner");

    // Mouse
    var mouse = {
        _x: 0,
        _y: 0,
        x: 0,
        y: 0,
        updatePosition: function(event) {
            var e = event || window.event;
            this.x = e.clientX - this._x;
            this.y = (e.clientY - this._y) * -1;
        },
        setOrigin: function(e) {
            this._x = e.offsetLeft + Math.floor(e.offsetWidth / 2);
            this._y = e.offsetTop + Math.floor(e.offsetHeight / 2);
        },
        show: function() {
            return "(" + this.x + ", " + this.y + ")";
        }
    };

    // Track the mouse position relative to the center of the container.
    mouse.setOrigin(container);

    //----------------------------------------------------

    var counter = 0;
    var refreshRate = 10;
    var isTimeToUpdate = function() {
        return counter++ % refreshRate === 0;
    };

    //----------------------------------------------------

    var onMouseEnterHandler = function(event) {
        update(event);
    };

    var onMouseLeaveHandler = function() {
        inner.style = "";
    };

    var onMouseMoveHandler = function(event) {
        if (isTimeToUpdate()) {
            update(event);
        }
    };

    //----------------------------------------------------

    var update = function(event) {
        mouse.updatePosition(event);
        updateTransformStyle(
            (mouse.y / inner.offsetHeight / 2).toFixed(2),
            (mouse.x / inner.offsetWidth / 2).toFixed(2)
        );
    };

    var updateTransformStyle = function(x, y) {
        var style = "rotateX(" + x + "deg) rotateY(" + y + "deg)";
        inner.style.transform = style;
        inner.style.webkitTransform = style;
        inner.style.mozTranform = style;
        inner.style.msTransform = style;
        inner.style.oTransform = style;
    };

    //--------------------------------------------------------

    container.onmousemove = onMouseMoveHandler;
    container.onmouseleave = onMouseLeaveHandler;
    container.onmouseenter = onMouseEnterHandler;
})();
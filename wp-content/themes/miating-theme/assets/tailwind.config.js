module.exports = {
  theme: {
    colors: {
      black: {
        '600': '#222',
        '800': '#111210',
      },
      grey: {
        '100': '#f9f9f9',
        '200': '#f0f0f0',
        '400': '#707070',
        '500': '#7b7b7b',
      },
      white: '#fff',
      primary: '#FAA900',
      secondary: '#053265'
    },
    extend: {
      fontSize: {
      },
      transitionProperty: {
        padding: 'padding'
      },
      gridTemplateColumns: {
        'menu': 'repeat(2, minmax(17.5rem, 1fr))',
      },
    }
  },
  variants: {
    textDecoration: ['responsive', 'hover', 'focus', 'group-hover'],
    textColor: ['responsive', 'hover', 'focus', 'active', 'group-hover'],
    width: ['responsive', 'group-hover'],
    opacity: ['responsive', 'hover', 'group-hover'],
    visibility: ['responsive', 'hover', 'group-hover'],
    boxShadow: ['responsive', 'hover', 'focus', 'active', 'group-hover'],
    display: ['responsive', 'group-hover'],
  },
  plugins: []
}

<?php
/**
 * This file adds the Global demo modal
 *
 * Gets data from options page
 *
 * @package Miating
 * @author  Ruben Zuidervaart
 * @license GPL-2.0+
 */

//Get the content
$all_options = get_acf_options();

$contact_details   = $all_options['contactgegevens'];

$phone_link = '';
$phone      = '';
$email      = '';

if($contact_details):

    $phone_link = $contact_details['phone_link'];
    $phone      = $contact_details['phone'];
    $email      = $contact_details['email'];

endif;

?>

<div class="NavSlidein nav-slidein absolute h-100 px-5 pt-12 pb-5 hidden">

    <div class="row no-gutters h-100">
        <div class="col align-self-start">
            <?php
            wp_nav_menu( array(
                    'theme_location' => 'slidein-menu',
                    'container'         => 'nav',
                    'menu_class'        => 'menu genesis-nav-menu folded-menu'
                )
            );
            ?>
        </div>

        <div class="col align-self-end text-right">
            <div class="">
                <a class="color--primary" href="tel:<?php echo $phone_link; ?>">
                    <?php echo $phone; ?>
                </a>
            </div>
            <div class="">
                <a class="color--primary" href="mailto:<?php echo $email; ?>">
                    <?php echo $email; ?>
                </a>
            </div>
        </div>
    </div>

</div>
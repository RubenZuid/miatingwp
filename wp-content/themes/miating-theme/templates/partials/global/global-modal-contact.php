<?php
/**
 * This file adds the Global demo modal
 *
 * Gets data from options page
 *
 * @package Miating
 * @author  Ruben Zuidervaart
 * @license GPL-2.0+
 */

//Get the content
$all_options    = get_acf_options();

$modal_contact   = $all_options['global_contact_modal'];

?>

<div class="modal modal__background">
</div>
<div class="modal modal-dialog modal-dialog-centered modal__contact" tabindex="-1" role="dialog" aria-labelledby="<?php echo $modal_contact['title']; ?>" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal__demo-dialog" role="document">
        <div class="modal-content modal__demo-content relative">
            <div class="modal__close absolute">
                <div class="modal__close--circle">
                    <div class="modal__close--circle-inner">

                    </div>
                </div>
            </div>
            <div class="modal-header border-bottom-0">
                <span class="h2 modal-title" id="">Neem contact met ons op</span>
            </div>
            <div class="modal-body py-0">
                <?php echo do_shortcode('[contact-form-7 id="551" title="Contact form 1"]'); ?>
            </div>
        </div>
    </div>
</div>
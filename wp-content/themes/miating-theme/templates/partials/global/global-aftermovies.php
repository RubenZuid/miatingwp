<?php

$all_fields = get_acf_fields(get_the_ID());

$aftermovies    = $all_fields['aftermovies'];

$aftermovies_title  = '';
$aftermovies_movies = '';

if( $aftermovies ):

    $aftermovies_title  = $aftermovies['title'];
    $aftermovies_movies = $aftermovies['aftermovies'];

endif;

?>

<section name="Aftermovies" class="">

    <div class="container">

        <?php
        //Do regular title template
        hm_get_template_part( '/templates/partials/global/small-parts/title-regular', [ 'color' => 'color--primary', 'content' => $aftermovies_title ] ); ?>

        <?php
        $rowClass   = 'row justify-content-around align-items-center pb-sm-5';
        ?>

        <div class="<?php echo $rowClass; ?>">
            <?php
            if($aftermovies_movies):
                $count = 1;
                foreach($aftermovies_movies as $aftermovie):

                    $aftermovie_link    = $aftermovie['link'];
                    $aftermovie_title   = $aftermovie_link['title'];
                    $aftermovie_url     = $aftermovie_link['url'];
                    $aftermovie_target  = $aftermovie_link['target'];

                    $aftermovie_image   = $aftermovie['image'];

                    ?>

                    <div class="col-12 col-sm text-center my-2 my-sm-0">
                        <div class="relative">
                            <img class="cardshadow lazy" data-src="<?php echo $aftermovie_image['url']; ?>" />
                            <a class="absolute overlay__content w-100 h-100"
                               target="<?php echo $aftermovie_target; ?>"
                               href="<?php echo $aftermovie_url; ?>" >
                            </a>
                        </div>
                    </div>

                    <?php
                    if( $count % 3 == 0):
                        echo '</div><div class="'. $rowClass .'">';
                    endif;

                    $count++;
                endforeach;
            endif;
            ?>
        </div>

    </div>

</section>

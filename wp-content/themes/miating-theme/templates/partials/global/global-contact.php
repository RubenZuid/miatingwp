<?php

$all_fields = get_acf_fields(get_the_ID());

$contact    = $all_fields['contact'];

$contact_title     = '';
$contact_backgr    = '';

if( $contact ):

    $contact_title     = $contact['title'];
    $contact_backgr    = $contact['background_image'];

endif;

?>

<section name="sponsors"
         class="mt-5 underglow"
         style="
                 background-image: url(<?php echo $contact_backgr['url'];  ?>);
                 background-repeat: no-repeat;
                 background-position: center;
                 background-size: cover;
                 ">

    <div class="py-5">

        <?php
        //Do regular title template
        hm_get_template_part( '/templates/partials/global/small-parts/title-regular', [ 'color' => 'color--white', 'content' => $contact_title ] ); ?>

        <div class="py-4 px-4 px-sm-0">
            <div class="">
                <div class="row justify-content-around align-items-center">
                    <div class="col-md-6 text-center">
                        <?php echo do_shortcode('[contact-form-7 id="551" title="Contact form 1"]'); ?>
                    </div>
                </div>
            </div>
        </div>

    </div>

</section>

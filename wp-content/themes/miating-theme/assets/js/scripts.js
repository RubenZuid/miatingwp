/**
 *
 * This will contain all the smaller JS files that would otherwise all have separate requests
 *
 **/

/*!
 * jQuery & Zepto Lazy - v1.7.10
 * http://jquery.eisbehr.de/lazy/
 *
 * Copyright 2012 - 2018, Daniel 'Eisbehr' Kern
 *
 * Dual licensed under the MIT and GPL-2.0 licenses:
 * http://www.opensource.org/licenses/mit-license.php
 * http://www.gnu.org/licenses/gpl-2.0.html
 *
 * $("img.lazy").lazy();
 */

;(function(window, undefined) {
    "use strict";

    // noinspection JSUnresolvedVariable
    /**
     * library instance - here and not in construct to be shorter in minimization
     * @return void
     */
    var $ = window.jQuery || window.Zepto,

        /**
         * unique plugin instance id counter
         * @type {number}
         */
        lazyInstanceId = 0,

        /**
         * helper to register window load for jQuery 3
         * @type {boolean}
         */
        windowLoaded = false;

    /**
     * make lazy available to jquery - and make it a bit more case-insensitive :)
     * @access public
     * @type {function}
     * @param {object} settings
     * @return {LazyPlugin}
     */
    $.fn.Lazy = $.fn.lazy = function(settings) {
        return new LazyPlugin(this, settings);
    };

    /**
     * helper to add plugins to lazy prototype configuration
     * @access public
     * @type {function}
     * @param {string|Array} names
     * @param {string|Array|function} [elements]
     * @param {function} loader
     * @return void
     */
    $.Lazy = $.lazy = function(names, elements, loader) {
        // make second parameter optional
        if ($.isFunction(elements)) {
            loader = elements;
            elements = [];
        }

        // exit here if parameter is not a callable function
        if (!$.isFunction(loader)) {
            return;
        }

        // make parameters an array of names to be sure
        names = $.isArray(names) ? names : [names];
        elements = $.isArray(elements) ? elements : [elements];

        var config = LazyPlugin.prototype.config,
            forced = config._f || (config._f = {});

        // add the loader plugin for every name
        for (var i = 0, l = names.length; i < l; i++) {
            if (config[names[i]] === undefined || $.isFunction(config[names[i]])) {
                config[names[i]] = loader;
            }
        }

        // add forced elements loader
        for (var c = 0, a = elements.length; c < a; c++) {
            forced[elements[c]] = names[0];
        }
    };

    /**
     * contains all logic and the whole element handling
     * is packed in a private function outside class to reduce memory usage, because it will not be created on every plugin instance
     * @access private
     * @type {function}
     * @param {LazyPlugin} instance
     * @param {object} config
     * @param {object|Array} items
     * @param {object} events
     * @param {string} namespace
     * @return void
     */
    function _executeLazy(instance, config, items, events, namespace) {
        /**
         * a helper to trigger the 'onFinishedAll' callback after all other events
         * @access private
         * @type {number}
         */
        var _awaitingAfterLoad = 0,

            /**
             * visible content width
             * @access private
             * @type {number}
             */
            _actualWidth = -1,

            /**
             * visible content height
             * @access private
             * @type {number}
             */
            _actualHeight = -1,

            /**
             * determine possibly detected high pixel density
             * @access private
             * @type {boolean}
             */
            _isRetinaDisplay = false,

            /**
             * dictionary entry for better minimization
             * @access private
             * @type {string}
             */
            _afterLoad = 'afterLoad',

            /**
             * dictionary entry for better minimization
             * @access private
             * @type {string}
             */
            _load = 'load',

            /**
             * dictionary entry for better minimization
             * @access private
             * @type {string}
             */
            _error = 'error',

            /**
             * dictionary entry for better minimization
             * @access private
             * @type {string}
             */
            _img = 'img',

            /**
             * dictionary entry for better minimization
             * @access private
             * @type {string}
             */
            _src = 'src',

            /**
             * dictionary entry for better minimization
             * @access private
             * @type {string}
             */
            _srcset = 'srcset',

            /**
             * dictionary entry for better minimization
             * @access private
             * @type {string}
             */
            _sizes = 'sizes',

            /**
             * dictionary entry for better minimization
             * @access private
             * @type {string}
             */
            _backgroundImage = 'background-image';

        /**
         * initialize plugin
         * bind loading to events or set delay time to load all items at once
         * @access private
         * @return void
         */
        function _initialize() {
            // detect actual device pixel ratio
            // noinspection JSUnresolvedVariable
            _isRetinaDisplay = window.devicePixelRatio > 1;

            // prepare all initial items
            items = _prepareItems(items);

            // if delay time is set load all items at once after delay time
            if (config.delay >= 0) {
                setTimeout(function() {
                    _lazyLoadItems(true);
                }, config.delay);
            }

            // if no delay is set or combine usage is active bind events
            if (config.delay < 0 || config.combined) {
                // create unique event function
                events.e = _throttle(config.throttle, function(event) {
                    // reset detected window size on resize event
                    if (event.type === 'resize') {
                        _actualWidth = _actualHeight = -1;
                    }

                    // execute 'lazy magic'
                    _lazyLoadItems(event.all);
                });

                // create function to add new items to instance
                events.a = function(additionalItems) {
                    additionalItems = _prepareItems(additionalItems);
                    items.push.apply(items, additionalItems);
                };

                // create function to get all instance items left
                events.g = function() {
                    // filter loaded items before return in case internal filter was not running until now
                    return (items = $(items).filter(function() {
                        return !$(this).data(config.loadedName);
                    }));
                };

                // create function to force loading elements
                events.f = function(forcedItems) {
                    for (var i = 0; i < forcedItems.length; i++) {
                        // only handle item if available in current instance
                        // use a compare function, because Zepto can't handle object parameter for filter
                        // var item = items.filter(forcedItems[i]);
                        /* jshint loopfunc: true */
                        var item = items.filter(function() {
                            return this === forcedItems[i];
                        });

                        if (item.length) {
                            _lazyLoadItems(false, item);
                        }
                    }
                };

                // load initial items
                _lazyLoadItems();

                // bind lazy load functions to scroll and resize event
                // noinspection JSUnresolvedVariable
                $(config.appendScroll).on('scroll.' + namespace + ' resize.' + namespace, events.e);
            }
        }

        /**
         * prepare items before handle them
         * @access private
         * @param {Array|object|jQuery} items
         * @return {Array|object|jQuery}
         */
        function _prepareItems(items) {
            // fetch used configurations before loops
            var defaultImage = config.defaultImage,
                placeholder = config.placeholder,
                imageBase = config.imageBase,
                srcsetAttribute = config.srcsetAttribute,
                loaderAttribute = config.loaderAttribute,
                forcedTags = config._f || {};

            // filter items and only add those who not handled yet and got needed attributes available
            items = $(items).filter(function() {
                var element = $(this),
                    tag = _getElementTagName(this);

                return !element.data(config.handledName) &&
                    (element.attr(config.attribute) || element.attr(srcsetAttribute) || element.attr(loaderAttribute) || forcedTags[tag] !== undefined);
            })

            // append plugin instance to all elements
                .data('plugin_' + config.name, instance);

            for (var i = 0, l = items.length; i < l; i++) {
                var element = $(items[i]),
                    tag = _getElementTagName(items[i]),
                    elementImageBase = element.attr(config.imageBaseAttribute) || imageBase;

                // generate and update source set if an image base is set
                if (tag === _img && elementImageBase && element.attr(srcsetAttribute)) {
                    element.attr(srcsetAttribute, _getCorrectedSrcSet(element.attr(srcsetAttribute), elementImageBase));
                }

                // add loader to forced element types
                if (forcedTags[tag] !== undefined && !element.attr(loaderAttribute)) {
                    element.attr(loaderAttribute, forcedTags[tag]);
                }

                // set default image on every element without source
                if (tag === _img && defaultImage && !element.attr(_src)) {
                    element.attr(_src, defaultImage);
                }

                // set placeholder on every element without background image
                else if (tag !== _img && placeholder && (!element.css(_backgroundImage) || element.css(_backgroundImage) === 'none')) {
                    element.css(_backgroundImage, "url('" + placeholder + "')");
                }
            }

            return items;
        }

        /**
         * the 'lazy magic' - check all items
         * @access private
         * @param {boolean} [allItems]
         * @param {object} [forced]
         * @return void
         */
        function _lazyLoadItems(allItems, forced) {
            // skip if no items where left
            if (!items.length) {
                // destroy instance if option is enabled
                if (config.autoDestroy) {
                    // noinspection JSUnresolvedFunction
                    instance.destroy();
                }

                return;
            }

            var elements = forced || items,
                loadTriggered = false,
                imageBase = config.imageBase || '',
                srcsetAttribute = config.srcsetAttribute,
                handledName = config.handledName;

            // loop all available items
            for (var i = 0; i < elements.length; i++) {
                // item is at least in loadable area
                if (allItems || forced || _isInLoadableArea(elements[i])) {
                    var element = $(elements[i]),
                        tag = _getElementTagName(elements[i]),
                        attribute = element.attr(config.attribute),
                        elementImageBase = element.attr(config.imageBaseAttribute) || imageBase,
                        customLoader = element.attr(config.loaderAttribute);

                    // is not already handled
                    if (!element.data(handledName) &&
                        // and is visible or visibility doesn't matter
                        (!config.visibleOnly || element.is(':visible')) && (
                            // and image source or source set attribute is available
                            (attribute || element.attr(srcsetAttribute)) && (
                                // and is image tag where attribute is not equal source or source set
                                (tag === _img && (elementImageBase + attribute !== element.attr(_src) || element.attr(srcsetAttribute) !== element.attr(_srcset))) ||
                                // or is non image tag where attribute is not equal background
                                (tag !== _img && elementImageBase + attribute !== element.css(_backgroundImage))
                            ) ||
                            // or custom loader is available
                            customLoader))
                    {
                        // mark element always as handled as this point to prevent double handling
                        loadTriggered = true;
                        element.data(handledName, true);

                        // load item
                        _handleItem(element, tag, elementImageBase, customLoader);
                    }
                }
            }

            // when something was loaded remove them from remaining items
            if (loadTriggered) {
                items = $(items).filter(function() {
                    return !$(this).data(handledName);
                });
            }
        }

        /**
         * load the given element the lazy way
         * @access private
         * @param {object} element
         * @param {string} tag
         * @param {string} imageBase
         * @param {function} [customLoader]
         * @return void
         */
        function _handleItem(element, tag, imageBase, customLoader) {
            // increment count of items waiting for after load
            ++_awaitingAfterLoad;

            // extended error callback for correct 'onFinishedAll' handling
            var errorCallback = function() {
                _triggerCallback('onError', element);
                _reduceAwaiting();

                // prevent further callback calls
                errorCallback = $.noop;
            };

            // trigger function before loading image
            _triggerCallback('beforeLoad', element);

            // fetch all double used data here for better code minimization
            var srcAttribute = config.attribute,
                srcsetAttribute = config.srcsetAttribute,
                sizesAttribute = config.sizesAttribute,
                retinaAttribute = config.retinaAttribute,
                removeAttribute = config.removeAttribute,
                loadedName = config.loadedName,
                elementRetina = element.attr(retinaAttribute);

            // handle custom loader
            if (customLoader) {
                // on load callback
                var loadCallback = function() {
                    // remove attribute from element
                    if (removeAttribute) {
                        element.removeAttr(config.loaderAttribute);
                    }

                    // mark element as loaded
                    element.data(loadedName, true);

                    // call after load event
                    _triggerCallback(_afterLoad, element);

                    // remove item from waiting queue and possibly trigger finished event
                    // it's needed to be asynchronous to run after filter was in _lazyLoadItems
                    setTimeout(_reduceAwaiting, 1);

                    // prevent further callback calls
                    loadCallback = $.noop;
                };

                // bind error event to trigger callback and reduce waiting amount
                element.off(_error).one(_error, errorCallback)

                // bind after load callback to element
                    .one(_load, loadCallback);

                // trigger custom loader and handle response
                if (!_triggerCallback(customLoader, element, function(response) {
                        if(response) {
                            element.off(_load);
                            loadCallback();
                        }
                        else {
                            element.off(_error);
                            errorCallback();
                        }
                    })) {
                    element.trigger(_error);
                }
            }

            // handle images
            else {
                // create image object
                var imageObj = $(new Image());

                // bind error event to trigger callback and reduce waiting amount
                imageObj.one(_error, errorCallback)

                // bind after load callback to image
                    .one(_load, function() {
                        // remove element from view
                        element.hide();

                        // set image back to element
                        // do it as single 'attr' calls, to be sure 'src' is set after 'srcset'
                        if (tag === _img) {
                            element.attr(_sizes, imageObj.attr(_sizes))
                                .attr(_srcset, imageObj.attr(_srcset))
                                .attr(_src, imageObj.attr(_src));
                        }
                        else {
                            element.css(_backgroundImage, "url('" + imageObj.attr(_src) + "')");
                        }

                        // bring it back with some effect!
                        element[config.effect](config.effectTime);

                        // remove attribute from element
                        if (removeAttribute) {
                            element.removeAttr(srcAttribute + ' ' + srcsetAttribute + ' ' + retinaAttribute + ' ' + config.imageBaseAttribute);

                            // only remove 'sizes' attribute, if it was a custom one
                            if (sizesAttribute !== _sizes) {
                                element.removeAttr(sizesAttribute);
                            }
                        }

                        // mark element as loaded
                        element.data(loadedName, true);

                        // call after load event
                        _triggerCallback(_afterLoad, element);

                        // cleanup image object
                        imageObj.remove();

                        // remove item from waiting queue and possibly trigger finished event
                        _reduceAwaiting();
                    });

                // set sources
                // do it as single 'attr' calls, to be sure 'src' is set after 'srcset'
                var imageSrc = (_isRetinaDisplay && elementRetina ? elementRetina : element.attr(srcAttribute)) || '';
                imageObj.attr(_sizes, element.attr(sizesAttribute))
                    .attr(_srcset, element.attr(srcsetAttribute))
                    .attr(_src, imageSrc ? imageBase + imageSrc : null);

                // call after load even on cached image
                imageObj.complete && imageObj.trigger(_load); // jshint ignore : line
            }
        }

        /**
         * check if the given element is inside the current viewport or threshold
         * @access private
         * @param {object} element
         * @return {boolean}
         */
        function _isInLoadableArea(element) {
            var elementBound = element.getBoundingClientRect(),
                direction    = config.scrollDirection,
                threshold    = config.threshold,
                vertical     = // check if element is in loadable area from top
                    ((_getActualHeight() + threshold) > elementBound.top) &&
                    // check if element is even in loadable are from bottom
                    (-threshold < elementBound.bottom),
                horizontal   = // check if element is in loadable area from left
                    ((_getActualWidth() + threshold) > elementBound.left) &&
                    // check if element is even in loadable area from right
                    (-threshold < elementBound.right);

            if (direction === 'vertical') {
                return vertical;
            }
            else if (direction === 'horizontal') {
                return horizontal;
            }

            return vertical && horizontal;
        }

        /**
         * receive the current viewed width of the browser
         * @access private
         * @return {number}
         */
        function _getActualWidth() {
            return _actualWidth >= 0 ? _actualWidth : (_actualWidth = $(window).width());
        }

        /**
         * receive the current viewed height of the browser
         * @access private
         * @return {number}
         */
        function _getActualHeight() {
            return _actualHeight >= 0 ? _actualHeight : (_actualHeight = $(window).height());
        }

        /**
         * get lowercase tag name of an element
         * @access private
         * @param {object} element
         * @returns {string}
         */
        function _getElementTagName(element) {
            return element.tagName.toLowerCase();
        }

        /**
         * prepend image base to all srcset entries
         * @access private
         * @param {string} srcset
         * @param {string} imageBase
         * @returns {string}
         */
        function _getCorrectedSrcSet(srcset, imageBase) {
            if (imageBase) {
                // trim, remove unnecessary spaces and split entries
                var entries = srcset.split(',');
                srcset = '';

                for (var i = 0, l = entries.length; i < l; i++) {
                    srcset += imageBase + entries[i].trim() + (i !== l - 1 ? ',' : '');
                }
            }

            return srcset;
        }

        /**
         * helper function to throttle down event triggering
         * @access private
         * @param {number} delay
         * @param {function} callback
         * @return {function}
         */
        function _throttle(delay, callback) {
            var timeout,
                lastExecute = 0;

            return function(event, ignoreThrottle) {
                var elapsed = +new Date() - lastExecute;

                function run() {
                    lastExecute = +new Date();
                    // noinspection JSUnresolvedFunction
                    callback.call(instance, event);
                }

                timeout && clearTimeout(timeout); // jshint ignore : line

                if (elapsed > delay || !config.enableThrottle || ignoreThrottle) {
                    run();
                }
                else {
                    timeout = setTimeout(run, delay - elapsed);
                }
            };
        }

        /**
         * reduce count of awaiting elements to 'afterLoad' event and fire 'onFinishedAll' if reached zero
         * @access private
         * @return void
         */
        function _reduceAwaiting() {
            --_awaitingAfterLoad;

            // if no items were left trigger finished event
            if (!items.length && !_awaitingAfterLoad) {
                _triggerCallback('onFinishedAll');
            }
        }

        /**
         * single implementation to handle callbacks, pass element and set 'this' to current instance
         * @access private
         * @param {string|function} callback
         * @param {object} [element]
         * @param {*} [args]
         * @return {boolean}
         */
        function _triggerCallback(callback, element, args) {
            if ((callback = config[callback])) {
                // jQuery's internal '$(arguments).slice(1)' are causing problems at least on old iPads
                // below is shorthand of 'Array.prototype.slice.call(arguments, 1)'
                callback.apply(instance, [].slice.call(arguments, 1));
                return true;
            }

            return false;
        }

        // if event driven or window is already loaded don't wait for page loading
        if (config.bind === 'event' || windowLoaded) {
            _initialize();
        }

        // otherwise load initial items and start lazy after page load
        else {
            // noinspection JSUnresolvedVariable
            $(window).on(_load + '.' + namespace, _initialize);
        }
    }

    /**
     * lazy plugin class constructor
     * @constructor
     * @access private
     * @param {object} elements
     * @param {object} settings
     * @return {object|LazyPlugin}
     */
    function LazyPlugin(elements, settings) {
        /**
         * this lazy plugin instance
         * @access private
         * @type {object|LazyPlugin|LazyPlugin.prototype}
         */
        var _instance = this,

            /**
             * this lazy plugin instance configuration
             * @access private
             * @type {object}
             */
            _config = $.extend({}, _instance.config, settings),

            /**
             * instance generated event executed on container scroll or resize
             * packed in an object to be referenceable and short named because properties will not be minified
             * @access private
             * @type {object}
             */
            _events = {},

            /**
             * unique namespace for instance related events
             * @access private
             * @type {string}
             */
            _namespace = _config.name + '-' + (++lazyInstanceId);

        // noinspection JSUndefinedPropertyAssignment
        /**
         * wrapper to get or set an entry from plugin instance configuration
         * much smaller on minify as direct access
         * @access public
         * @type {function}
         * @param {string} entryName
         * @param {*} [value]
         * @return {LazyPlugin|*}
         */
        _instance.config = function(entryName, value) {
            if (value === undefined) {
                return _config[entryName];
            }

            _config[entryName] = value;
            return _instance;
        };

        // noinspection JSUndefinedPropertyAssignment
        /**
         * add additional items to current instance
         * @access public
         * @param {Array|object|string} items
         * @return {LazyPlugin}
         */
        _instance.addItems = function(items) {
            _events.a && _events.a($.type(items) === 'string' ? $(items) : items); // jshint ignore : line
            return _instance;
        };

        // noinspection JSUndefinedPropertyAssignment
        /**
         * get all left items of this instance
         * @access public
         * @returns {object}
         */
        _instance.getItems = function() {
            return _events.g ? _events.g() : {};
        };

        // noinspection JSUndefinedPropertyAssignment
        /**
         * force lazy to load all items in loadable area right now
         * by default without throttle
         * @access public
         * @type {function}
         * @param {boolean} [useThrottle]
         * @return {LazyPlugin}
         */
        _instance.update = function(useThrottle) {
            _events.e && _events.e({}, !useThrottle); // jshint ignore : line
            return _instance;
        };

        // noinspection JSUndefinedPropertyAssignment
        /**
         * force element(s) to load directly, ignoring the viewport
         * @access public
         * @param {Array|object|string} items
         * @return {LazyPlugin}
         */
        _instance.force = function(items) {
            _events.f && _events.f($.type(items) === 'string' ? $(items) : items); // jshint ignore : line
            return _instance;
        };

        // noinspection JSUndefinedPropertyAssignment
        /**
         * force lazy to load all available items right now
         * this call ignores throttling
         * @access public
         * @type {function}
         * @return {LazyPlugin}
         */
        _instance.loadAll = function() {
            _events.e && _events.e({all: true}, true); // jshint ignore : line
            return _instance;
        };

        // noinspection JSUndefinedPropertyAssignment
        /**
         * destroy this plugin instance
         * @access public
         * @type {function}
         * @return undefined
         */
        _instance.destroy = function() {
            // unbind instance generated events
            // noinspection JSUnresolvedFunction, JSUnresolvedVariable
            $(_config.appendScroll).off('.' + _namespace, _events.e);
            // noinspection JSUnresolvedVariable
            $(window).off('.' + _namespace);

            // clear events
            _events = {};

            return undefined;
        };

        // start using lazy and return all elements to be chainable or instance for further use
        // noinspection JSUnresolvedVariable
        _executeLazy(_instance, _config, elements, _events, _namespace);
        return _config.chainable ? elements : _instance;
    }

    /**
     * settings and configuration data
     * @access public
     * @type {object|*}
     */
    LazyPlugin.prototype.config = {
        // general
        name               : 'lazy',
        chainable          : true,
        autoDestroy        : true,
        bind               : 'load',
        threshold          : 500,
        visibleOnly        : false,
        appendScroll       : window,
        scrollDirection    : 'both',
        imageBase          : null,
        defaultImage       : 'data:image/gif;base64,R0lGODlhAQABAIAAAP///wAAACH5BAEAAAAALAAAAAABAAEAAAICRAEAOw==',
        placeholder        : null,
        delay              : -1,
        combined           : false,

        // attributes
        attribute          : 'data-src',
        srcsetAttribute    : 'data-srcset',
        sizesAttribute     : 'data-sizes',
        retinaAttribute    : 'data-retina',
        loaderAttribute    : 'data-loader',
        imageBaseAttribute : 'data-imagebase',
        removeAttribute    : true,
        handledName        : 'handled',
        loadedName         : 'loaded',

        // effect
        effect             : 'show',
        effectTime         : 0,

        // throttle
        enableThrottle     : true,
        throttle           : 250,

        // callbacks
        beforeLoad         : undefined,
        afterLoad          : undefined,
        onError            : undefined,
        onFinishedAll      : undefined
    };

    // register window load event globally to prevent not loading elements
    // since jQuery 3.X ready state is fully async and may be executed after 'load'
    $(window).on('load', function() {
        windowLoaded = true;
    });
})(window);


/*!
 * jQuery & Zepto Lazy - iFrame Plugin - v1.5
 * http://jquery.eisbehr.de/lazy/
 *
 * Copyright 2012 - 2018, Daniel 'Eisbehr' Kern
 *
 * Dual licensed under the MIT and GPL-2.0 licenses:
 * http://www.opensource.org/licenses/mit-license.php
 * http://www.gnu.org/licenses/gpl-2.0.html
 */
;(function($) {
    // load iframe content, like:
    // <iframe data-src="iframe.html"></iframe>
    //
    // enable content error check with:
    // <iframe data-src="iframe.html" data-error-detect="true"></iframe>
    $.lazy(['frame', 'iframe'], 'iframe', function(element, response) {
        var instance = this;

        if (element[0].tagName.toLowerCase() === 'iframe') {
            var srcAttr = 'data-src',
                errorDetectAttr = 'data-error-detect',
                errorDetect = element.attr(errorDetectAttr);

            // default way, just replace the 'src' attribute
            if (errorDetect !== 'true' && errorDetect !== '1') {
                // set iframe source
                element.attr('src', element.attr(srcAttr));

                // remove attributes
                if (instance.config('removeAttribute')) {
                    element.removeAttr(srcAttr + ' ' + errorDetectAttr);
                }
            }

            // extended way, even check if the document is available
            else {
                $.ajax({
                    url: element.attr(srcAttr),
                    dataType: 'html',
                    crossDomain: true,
                    xhrFields: {withCredentials: true},

                    /**
                     * success callback
                     * @access private
                     * @param {*} content
                     * @return {void}
                     */
                    success: function(content) {
                        // set responded data to element's inner html
                        element.html(content)

                        // change iframe src
                            .attr('src', element.attr(srcAttr));

                        // remove attributes
                        if (instance.config('removeAttribute')) {
                            element.removeAttr(srcAttr + ' ' + errorDetectAttr);
                        }
                    },

                    /**
                     * error callback
                     * @access private
                     * @return {void}
                     */
                    error: function() {
                        // pass error state to lazy
                        // use response function for Zepto
                        response(false);
                    }
                });
            }
        }

        else {
            // pass error state to lazy
            // use response function for Zepto
            response(false);
        }
    });
})(window.jQuery || window.Zepto);

/*
Contact forms 7
 */

( function( $ ) {

    'use strict';

    if ( typeof wpcf7 === 'undefined' || wpcf7 === null ) {
        return;
    }

    wpcf7 = $.extend( {
        cached: 0,
        inputs: []
    }, wpcf7 );

    $( function() {
        wpcf7.supportHtml5 = ( function() {
            var features = {};
            var input = document.createElement( 'input' );

            features.placeholder = 'placeholder' in input;

            var inputTypes = [ 'email', 'url', 'tel', 'number', 'range', 'date' ];

            $.each( inputTypes, function( index, value ) {
                input.setAttribute( 'type', value );
                features[ value ] = input.type !== 'text';
            } );

            return features;
        } )();

        $( 'div.wpcf7 > form' ).each( function() {
            var $form = $( this );
            wpcf7.initForm( $form );

            if ( wpcf7.cached ) {
                wpcf7.refill( $form );
            }
        } );
    } );

    wpcf7.getId = function( form ) {
        return parseInt( $( 'input[name="_wpcf7"]', form ).val(), 10 );
    };

    wpcf7.initForm = function( form ) {
        var $form = $( form );

        $form.submit( function( event ) {
            if ( ! wpcf7.supportHtml5.placeholder ) {
                $( '[placeholder].placeheld', $form ).each( function( i, n ) {
                    $( n ).val( '' ).removeClass( 'placeheld' );
                } );
            }

            if ( typeof window.FormData === 'function' ) {
                wpcf7.submit( $form );
                event.preventDefault();
            }
        } );

        $( '.wpcf7-submit', $form ).after( '<span class="ajax-loader"></span>' );

        wpcf7.toggleSubmit( $form );

        $form.on( 'click', '.wpcf7-acceptance', function() {
            wpcf7.toggleSubmit( $form );
        } );

        // Exclusive Checkbox
        $( '.wpcf7-exclusive-checkbox', $form ).on( 'click', 'input:checkbox', function() {
            var name = $( this ).attr( 'name' );
            $form.find( 'input:checkbox[name="' + name + '"]' ).not( this ).prop( 'checked', false );
        } );

        // Free Text Option for Checkboxes and Radio Buttons
        $( '.wpcf7-list-item.has-free-text', $form ).each( function() {
            var $freetext = $( ':input.wpcf7-free-text', this );
            var $wrap = $( this ).closest( '.wpcf7-form-control' );

            if ( $( ':checkbox, :radio', this ).is( ':checked' ) ) {
                $freetext.prop( 'disabled', false );
            } else {
                $freetext.prop( 'disabled', true );
            }

            $wrap.on( 'change', ':checkbox, :radio', function() {
                var $cb = $( '.has-free-text', $wrap ).find( ':checkbox, :radio' );

                if ( $cb.is( ':checked' ) ) {
                    $freetext.prop( 'disabled', false ).focus();
                } else {
                    $freetext.prop( 'disabled', true );
                }
            } );
        } );

        // Placeholder Fallback
        if ( ! wpcf7.supportHtml5.placeholder ) {
            $( '[placeholder]', $form ).each( function() {
                $( this ).val( $( this ).attr( 'placeholder' ) );
                $( this ).addClass( 'placeheld' );

                $( this ).focus( function() {
                    if ( $( this ).hasClass( 'placeheld' ) ) {
                        $( this ).val( '' ).removeClass( 'placeheld' );
                    }
                } );

                $( this ).blur( function() {
                    if ( '' === $( this ).val() ) {
                        $( this ).val( $( this ).attr( 'placeholder' ) );
                        $( this ).addClass( 'placeheld' );
                    }
                } );
            } );
        }

        if ( wpcf7.jqueryUi && ! wpcf7.supportHtml5.date ) {
            $form.find( 'input.wpcf7-date[type="date"]' ).each( function() {
                $( this ).datepicker( {
                    dateFormat: 'yy-mm-dd',
                    minDate: new Date( $( this ).attr( 'min' ) ),
                    maxDate: new Date( $( this ).attr( 'max' ) )
                } );
            } );
        }

        if ( wpcf7.jqueryUi && ! wpcf7.supportHtml5.number ) {
            $form.find( 'input.wpcf7-number[type="number"]' ).each( function() {
                $( this ).spinner( {
                    min: $( this ).attr( 'min' ),
                    max: $( this ).attr( 'max' ),
                    step: $( this ).attr( 'step' )
                } );
            } );
        }

        // Character Count
        $( '.wpcf7-character-count', $form ).each( function() {
            var $count = $( this );
            var name = $count.attr( 'data-target-name' );
            var down = $count.hasClass( 'down' );
            var starting = parseInt( $count.attr( 'data-starting-value' ), 10 );
            var maximum = parseInt( $count.attr( 'data-maximum-value' ), 10 );
            var minimum = parseInt( $count.attr( 'data-minimum-value' ), 10 );

            var updateCount = function( target ) {
                var $target = $( target );
                var length = $target.val().length;
                var count = down ? starting - length : length;
                $count.attr( 'data-current-value', count );
                $count.text( count );

                if ( maximum && maximum < length ) {
                    $count.addClass( 'too-long' );
                } else {
                    $count.removeClass( 'too-long' );
                }

                if ( minimum && length < minimum ) {
                    $count.addClass( 'too-short' );
                } else {
                    $count.removeClass( 'too-short' );
                }
            };

            $( ':input[name="' + name + '"]', $form ).each( function() {
                updateCount( this );

                $( this ).keyup( function() {
                    updateCount( this );
                } );
            } );
        } );

        // URL Input Correction
        $form.on( 'change', '.wpcf7-validates-as-url', function() {
            var val = $.trim( $( this ).val() );

            if ( val
                && ! val.match( /^[a-z][a-z0-9.+-]*:/i )
                && -1 !== val.indexOf( '.' ) ) {
                val = val.replace( /^\/+/, '' );
                val = 'http://' + val;
            }

            $( this ).val( val );
        } );
    };

    wpcf7.submit = function( form ) {
        if ( typeof window.FormData !== 'function' ) {
            return;
        }

        var $form = $( form );

        $( '.ajax-loader', $form ).addClass( 'is-active' );

        wpcf7.clearResponse( $form );

        var formData = new FormData( $form.get( 0 ) );

        var detail = {
            id: $form.closest( 'div.wpcf7' ).attr( 'id' ),
            status: 'init',
            inputs: [],
            formData: formData
        };

        $.each( $form.serializeArray(), function( i, field ) {
            if ( '_wpcf7' == field.name ) {
                detail.contactFormId = field.value;
            } else if ( '_wpcf7_version' == field.name ) {
                detail.pluginVersion = field.value;
            } else if ( '_wpcf7_locale' == field.name ) {
                detail.contactFormLocale = field.value;
            } else if ( '_wpcf7_unit_tag' == field.name ) {
                detail.unitTag = field.value;
            } else if ( '_wpcf7_container_post' == field.name ) {
                detail.containerPostId = field.value;
            } else if ( field.name.match( /^_wpcf7_\w+_free_text_/ ) ) {
                var owner = field.name.replace( /^_wpcf7_\w+_free_text_/, '' );
                detail.inputs.push( {
                    name: owner + '-free-text',
                    value: field.value
                } );
            } else if ( field.name.match( /^_/ ) ) {
                // do nothing
            } else {
                detail.inputs.push( field );
            }
        } );

        wpcf7.triggerEvent( $form.closest( 'div.wpcf7' ), 'beforesubmit', detail );

        var ajaxSuccess = function( data, status, xhr, $form ) {
            detail.id = $( data.into ).attr( 'id' );
            detail.status = data.status;
            detail.apiResponse = data;

            var $message = $( '.wpcf7-response-output', $form );

            switch ( data.status ) {
                case 'validation_failed':
                    $.each( data.invalidFields, function( i, n ) {
                        $( n.into, $form ).each( function() {
                            wpcf7.notValidTip( this, n.message );
                            $( '.wpcf7-form-control', this ).addClass( 'wpcf7-not-valid' );
                            $( '[aria-invalid]', this ).attr( 'aria-invalid', 'true' );
                        } );
                    } );

                    $message.addClass( 'wpcf7-validation-errors' );
                    $form.addClass( 'invalid' );

                    wpcf7.triggerEvent( data.into, 'invalid', detail );
                    break;
                case 'acceptance_missing':
                    $message.addClass( 'wpcf7-acceptance-missing' );
                    $form.addClass( 'unaccepted' );

                    wpcf7.triggerEvent( data.into, 'unaccepted', detail );
                    break;
                case 'spam':
                    $message.addClass( 'wpcf7-spam-blocked' );
                    $form.addClass( 'spam' );

                    wpcf7.triggerEvent( data.into, 'spam', detail );
                    break;
                case 'aborted':
                    $message.addClass( 'wpcf7-aborted' );
                    $form.addClass( 'aborted' );

                    wpcf7.triggerEvent( data.into, 'aborted', detail );
                    break;
                case 'mail_sent':
                    $message.addClass( 'wpcf7-mail-sent-ok' );
                    $form.addClass( 'sent' );

                    wpcf7.triggerEvent( data.into, 'mailsent', detail );
                    break;
                case 'mail_failed':
                    $message.addClass( 'wpcf7-mail-sent-ng' );
                    $form.addClass( 'failed' );

                    wpcf7.triggerEvent( data.into, 'mailfailed', detail );
                    break;
                default:
                    var customStatusClass = 'custom-'
                        + data.status.replace( /[^0-9a-z]+/i, '-' );
                    $message.addClass( 'wpcf7-' + customStatusClass );
                    $form.addClass( customStatusClass );
            }

            wpcf7.refill( $form, data );

            wpcf7.triggerEvent( data.into, 'submit', detail );

            if ( 'mail_sent' == data.status ) {
                $form.each( function() {
                    this.reset();
                } );

                wpcf7.toggleSubmit( $form );
            }

            if ( ! wpcf7.supportHtml5.placeholder ) {
                $form.find( '[placeholder].placeheld' ).each( function( i, n ) {
                    $( n ).val( $( n ).attr( 'placeholder' ) );
                } );
            }

            $message.html( '' ).append( data.message ).slideDown( 'fast' );
            $message.attr( 'role', 'alert' );

            $( '.screen-reader-response', $form.closest( '.wpcf7' ) ).each( function() {
                var $response = $( this );
                $response.html( '' ).attr( 'role', '' ).append( data.message );

                if ( data.invalidFields ) {
                    var $invalids = $( '<ul></ul>' );

                    $.each( data.invalidFields, function( i, n ) {
                        if ( n.idref ) {
                            var $li = $( '<li></li>' ).append( $( '<a></a>' ).attr( 'href', '#' + n.idref ).append( n.message ) );
                        } else {
                            var $li = $( '<li></li>' ).append( n.message );
                        }

                        $invalids.append( $li );
                    } );

                    $response.append( $invalids );
                }

                $response.attr( 'role', 'alert' ).focus();
            } );
        };

        $.ajax( {
            type: 'POST',
            url: wpcf7.apiSettings.getRoute(
                '/contact-forms/' + wpcf7.getId( $form ) + '/feedback' ),
            data: formData,
            dataType: 'json',
            processData: false,
            contentType: false
        } ).done( function( data, status, xhr ) {
            ajaxSuccess( data, status, xhr, $form );
            $( '.ajax-loader', $form ).removeClass( 'is-active' );
        } ).fail( function( xhr, status, error ) {
            var $e = $( '<div class="ajax-error"></div>' ).text( error.message );
            $form.after( $e );
        } );
    };

    wpcf7.triggerEvent = function( target, name, detail ) {
        var $target = $( target );

        /* DOM event */
        var event = new CustomEvent( 'wpcf7' + name, {
            bubbles: true,
            detail: detail
        } );

        $target.get( 0 ).dispatchEvent( event );

        /* jQuery event */
        $target.trigger( 'wpcf7:' + name, detail );
        $target.trigger( name + '.wpcf7', detail ); // deprecated
    };

    wpcf7.toggleSubmit = function( form, state ) {
        var $form = $( form );
        var $submit = $( 'input:submit', $form );

        if ( typeof state !== 'undefined' ) {
            $submit.prop( 'disabled', ! state );
            return;
        }

        if ( $form.hasClass( 'wpcf7-acceptance-as-validation' ) ) {
            return;
        }

        $submit.prop( 'disabled', false );

        $( '.wpcf7-acceptance', $form ).each( function() {
            var $span = $( this );
            var $input = $( 'input:checkbox', $span );

            if ( ! $span.hasClass( 'optional' ) ) {
                if ( $span.hasClass( 'invert' ) && $input.is( ':checked' )
                    || ! $span.hasClass( 'invert' ) && ! $input.is( ':checked' ) ) {
                    $submit.prop( 'disabled', true );
                    return false;
                }
            }
        } );
    };

    wpcf7.notValidTip = function( target, message ) {
        var $target = $( target );
        $( '.wpcf7-not-valid-tip', $target ).remove();
        $( '<span role="alert" class="wpcf7-not-valid-tip"></span>' )
            .text( message ).appendTo( $target );

        if ( $target.is( '.use-floating-validation-tip *' ) ) {
            var fadeOut = function( target ) {
                $( target ).not( ':hidden' ).animate( {
                    opacity: 0
                }, 'fast', function() {
                    $( this ).css( { 'z-index': -100 } );
                } );
            };

            $target.on( 'mouseover', '.wpcf7-not-valid-tip', function() {
                fadeOut( this );
            } );

            $target.on( 'focus', ':input', function() {
                fadeOut( $( '.wpcf7-not-valid-tip', $target ) );
            } );
        }
    };

    wpcf7.refill = function( form, data ) {
        var $form = $( form );

        var refillCaptcha = function( $form, items ) {
            $.each( items, function( i, n ) {
                $form.find( ':input[name="' + i + '"]' ).val( '' );
                $form.find( 'img.wpcf7-captcha-' + i ).attr( 'src', n );
                var match = /([0-9]+)\.(png|gif|jpeg)$/.exec( n );
                $form.find( 'input:hidden[name="_wpcf7_captcha_challenge_' + i + '"]' ).attr( 'value', match[ 1 ] );
            } );
        };

        var refillQuiz = function( $form, items ) {
            $.each( items, function( i, n ) {
                $form.find( ':input[name="' + i + '"]' ).val( '' );
                $form.find( ':input[name="' + i + '"]' ).siblings( 'span.wpcf7-quiz-label' ).text( n[ 0 ] );
                $form.find( 'input:hidden[name="_wpcf7_quiz_answer_' + i + '"]' ).attr( 'value', n[ 1 ] );
            } );
        };

        if ( typeof data === 'undefined' ) {
            $.ajax( {
                type: 'GET',
                url: wpcf7.apiSettings.getRoute(
                    '/contact-forms/' + wpcf7.getId( $form ) + '/refill' ),
                beforeSend: function( xhr ) {
                    var nonce = $form.find( ':input[name="_wpnonce"]' ).val();

                    if ( nonce ) {
                        xhr.setRequestHeader( 'X-WP-Nonce', nonce );
                    }
                },
                dataType: 'json'
            } ).done( function( data, status, xhr ) {
                if ( data.captcha ) {
                    refillCaptcha( $form, data.captcha );
                }

                if ( data.quiz ) {
                    refillQuiz( $form, data.quiz );
                }
            } );

        } else {
            if ( data.captcha ) {
                refillCaptcha( $form, data.captcha );
            }

            if ( data.quiz ) {
                refillQuiz( $form, data.quiz );
            }
        }
    };

    wpcf7.clearResponse = function( form ) {
        var $form = $( form );
        $form.removeClass( 'invalid spam sent failed' );
        $form.siblings( '.screen-reader-response' ).html( '' ).attr( 'role', '' );

        $( '.wpcf7-not-valid-tip', $form ).remove();
        $( '[aria-invalid]', $form ).attr( 'aria-invalid', 'false' );
        $( '.wpcf7-form-control', $form ).removeClass( 'wpcf7-not-valid' );

        $( '.wpcf7-response-output', $form )
            .hide().empty().removeAttr( 'role' )
            .removeClass( 'wpcf7-mail-sent-ok wpcf7-mail-sent-ng wpcf7-validation-errors wpcf7-spam-blocked' );
    };

    wpcf7.apiSettings.getRoute = function( path ) {
        var url = wpcf7.apiSettings.root;

        url = url.replace(
            wpcf7.apiSettings.namespace,
            wpcf7.apiSettings.namespace + path );

        return url;
    };

} )( jQuery );

/*
 * Polyfill for Internet Explorer
 * See https://developer.mozilla.org/en-US/docs/Web/API/CustomEvent/CustomEvent
 */
( function () {
    if ( typeof window.CustomEvent === "function" ) return false;

    function CustomEvent ( event, params ) {
        params = params || { bubbles: false, cancelable: false, detail: undefined };
        var evt = document.createEvent( 'CustomEvent' );
        evt.initCustomEvent( event,
            params.bubbles, params.cancelable, params.detail );
        return evt;
    }

    CustomEvent.prototype = window.Event.prototype;

    window.CustomEvent = CustomEvent;
} )();

/**
 *
 * Genesis Skip links
 *
 * Callback to fix skip links focus.
 *
 * @since 2.2.0
 */
function ga_skiplinks() {
    'use strict';
    var fragmentID = location.hash.substring( 1 );
    if ( ! fragmentID ) {
        return;
    }

    var element = document.getElementById( fragmentID );
    if ( element ) {
        if ( false === /^(?:a|select|input|button|textarea)$/i.test( element.tagName ) ) {
            element.tabIndex = -1;
        }
        element.focus();
    }
}

if ( window.addEventListener ) {
    window.addEventListener( 'hashchange', ga_skiplinks, false );
} else { // IE8 and earlier.
    window.attachEvent( 'onhashchange', ga_skiplinks );
}


/**
 * jQuery plugin paroller.js v1.4.4
 * https://github.com/tgomilar/paroller.js
 * preview: https://tgomilar.github.io/paroller/
 **/
(function (factory) {
    'use strict';
    if (typeof define === 'function' && define.amd) {
        define('parollerjs', ['jquery'], factory);
    } else if (typeof module === 'object' && typeof module.exports === 'object') {
        module.exports = factory(require('jquery'));
    }
    else {
        factory(jQuery);
    }
})(function ($) {
    'use strict';

    var working = false;
    var scrollAction = function() {
        working = false;
    };

    var setDirection = {
        bgVertical: function (elem, bgOffset) {
            return elem.css({'background-position': 'center ' + -bgOffset + 'px'});
        },
        bgHorizontal: function (elem, bgOffset) {
            return elem.css({'background-position': -bgOffset + 'px' + ' center'});
        },
        vertical: function (elem, elemOffset, oldTransform) {
            (oldTransform === 'none' ? oldTransform = '' : true);
            return elem.css({
                '-webkit-transform': 'translateY(' + elemOffset + 'px)' + oldTransform,
                '-moz-transform': 'translateY(' + elemOffset + 'px)' + oldTransform,
                'transform': 'translateY(' + elemOffset + 'px)' + oldTransform,
                'transition': 'transform linear',
                'will-change': 'transform'
            });
        },
        horizontal: function (elem, elemOffset, oldTransform) {
            (oldTransform === 'none' ? oldTransform = '' : true);
            return elem.css({
                '-webkit-transform': 'translateX(' + elemOffset + 'px)' + oldTransform,
                '-moz-transform': 'translateX(' + elemOffset + 'px)' + oldTransform,
                'transform': 'translateX(' + elemOffset + 'px)' + oldTransform,
                'transition': 'transform linear',
                'will-change': 'transform'
            });
        }
    };

    var setMovement = {
        factor: function (elem, width, options) {
            var dataFactor = elem.data('paroller-factor');
            var factor = (dataFactor) ? dataFactor : options.factor;
            if (width < 576) {
                var dataFactorXs = elem.data('paroller-factor-xs');
                var factorXs = (dataFactorXs) ? dataFactorXs : options.factorXs;
                return (factorXs) ? factorXs : factor;
            }
            else if (width <= 768) {
                var dataFactorSm = elem.data('paroller-factor-sm');
                var factorSm = (dataFactorSm) ? dataFactorSm : options.factorSm;
                return (factorSm) ? factorSm : factor;
            }
            else if (width <= 1024) {
                var dataFactorMd = elem.data('paroller-factor-md');
                var factorMd = (dataFactorMd) ? dataFactorMd : options.factorMd;
                return (factorMd) ? factorMd : factor;
            }
            else if (width <= 1200) {
                var dataFactorLg = elem.data('paroller-factor-lg');
                var factorLg = (dataFactorLg) ? dataFactorLg : options.factorLg;
                return (factorLg) ? factorLg : factor;
            } else if (width <= 1920) {
                var dataFactorXl = elem.data('paroller-factor-xl');
                var factorXl = (dataFactorXl) ? dataFactorXl : options.factorXl;
                return (factorXl) ? factorXl : factor;
            } else {
                return factor;
            }
        },
        bgOffset: function (offset, factor) {
            return Math.round(offset * factor);
        },
        transform: function (offset, factor, windowHeight, height) {
            return Math.round((offset - (windowHeight / 2) + height) * factor);
        }
    };

    var clearPositions = {
        background: function (elem) {
            return elem.css({'background-position': 'unset'});
        },
        foreground: function (elem) {
            return elem.css({
                'transform' : 'unset',
                'transition' : 'unset'
            });
        }
    };

    $.fn.paroller = function (options) {
        var windowHeight = $(window).height();
        var documentHeight = $(document).height();

        // default options
        var options = $.extend({
            factor: 0, // - to +
            factorXs: 0, // - to +
            factorSm: 0, // - to +
            factorMd: 0, // - to +
            factorLg: 0, // - to +
            factorXl: 0, // - to +
            type: 'background', // foreground
            direction: 'vertical' // horizontal
        }, options);

        return this.each(function () {
            var $this = $(this);
            var width = $(window).width();
            var offset = $this.offset().top;
            var height = $this.outerHeight();

            var dataType = $this.data('paroller-type');
            var dataDirection = $this.data('paroller-direction');
            var oldTransform = $this.css('transform');

            var type = (dataType) ? dataType : options.type;
            var direction = (dataDirection) ? dataDirection : options.direction;
            var factor = setMovement.factor($this, width, options);
            var bgOffset = setMovement.bgOffset(offset, factor);
            var transform = setMovement.transform(offset, factor, windowHeight, height);

            if (type === 'background') {
                if (direction === 'vertical') {
                    setDirection.bgVertical($this, bgOffset);
                }
                else if (direction === 'horizontal') {
                    setDirection.bgHorizontal($this, bgOffset);
                }
            }
            else if (type === 'foreground') {
                if (direction === 'vertical') {
                    setDirection.vertical($this, transform, oldTransform);
                }
                else if (direction === 'horizontal') {
                    setDirection.horizontal($this, transform, oldTransform);
                }
            }

            $(window).on('resize', function () {
                var scrolling = $(this).scrollTop();
                width = $(window).width();
                offset = $this.offset().top;
                height = $this.outerHeight();
                factor = setMovement.factor($this, width, options);

                bgOffset = Math.round(offset * factor);
                transform = Math.round((offset - (windowHeight / 2) + height) * factor);

                if (! working) {
                    window.requestAnimationFrame(scrollAction);
                    working = true;
                }

                if (type === 'background') {
                    clearPositions.background($this);
                    if (direction === 'vertical') {
                        setDirection.bgVertical($this, bgOffset);
                    }
                    else if (direction === 'horizontal') {
                        setDirection.bgHorizontal($this, bgOffset);
                    }
                }
                else if ((type === 'foreground') && (scrolling <= documentHeight)) {
                    clearPositions.foreground($this);
                    if (direction === 'vertical') {
                        setDirection.vertical($this, transform);
                    }
                    else if (direction === 'horizontal') {
                        setDirection.horizontal($this, transform);
                    }
                }
            });

            $(window).on('scroll', function () {
                var scrolling = $(this).scrollTop();
                documentHeight = $(document).height();

                bgOffset = Math.round((offset - scrolling) * factor);
                transform = Math.round(((offset - (windowHeight / 2) + height) - scrolling) * factor);

                if (! working) {
                    window.requestAnimationFrame(scrollAction);
                    working = true;
                }

                if (type === 'background') {
                    if (direction === 'vertical') {
                        setDirection.bgVertical($this, bgOffset);
                    }
                    else if (direction === 'horizontal') {
                        setDirection.bgHorizontal($this, bgOffset);
                    }
                }
                else if ((type === 'foreground') && (scrolling <= documentHeight)) {
                    if (direction === 'vertical') {
                        setDirection.vertical($this, transform, oldTransform);
                    }
                    else if (direction === 'horizontal') {
                        setDirection.horizontal($this, transform, oldTransform);
                    }
                }
            });
        });
    };
});
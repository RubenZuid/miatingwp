<?php
/**
 * This file adds the overlapping cta block template.
 *
 * Gets data from options page
 *
 * @package MyDataFactory
 * @author  Ruben Zuidervaart
 * @license GPL-2.0+
 */

//Get the global form content
$all_options    = get_acf_options();
$all_fields     = get_acf_fields(get_the_ID());

$default_form_content   = $all_options['default_contact_text'];

//Setup new empty fields
$default_title          = '';
$default_description    = '';

//Fill the content
if( $default_form_content ): //Enter group

    $default_title          = $default_form_content['title'];
    $default_description    = $default_form_content['description'];

endif;

//Get the global contact details
$contact_details    = $all_options['contactgegevens'];

$contact_details_phone      = '';
$contact_details_phone_link = '';
$contact_details_email      = '';

$contact_person_name        = '';
$contact_person_image       = '';

if( $contact_details ): //Enter group
    $contact_details_phone      = $contact_details['phone'];
    $contact_details_phone_link = $contact_details['phone_link'];
    $contact_details_email      = $contact_details['email'];

    if ( is_singular( 'careers' ) ) {
        $contact_person = $contact_details['contactpersoon_recruitment'];
        if( $contact_person ): //Enter group

            $contact_person_name        = $contact_person['naam'];
            $contact_person_image       = $contact_person['foto'];

            $contact_details_phone      = $contact_person['phone'];
            $contact_details_phone_link = $contact_person['phone_link'];

        endif;
    } else {
        $contact_person = $contact_details['contactpersoon'];
        if( $contact_person ): //Enter group

            $contact_person_name        = $contact_person['naam'];
            $contact_person_image       = $contact_person['foto'];

        endif;
    }

endif;

//Get the content
$address    = $all_options['adresgegevens'];


//Setup new empty fields
$address_street         = '';
$address_postcode       = '';
$address_city           = '';
$address_country        = '';

//Fill the content
if( $address ):

    $address_street         = $address['straatnaam_huisnummer'];
    $address_postcode       = $address['postcode'];
    $address_city           = $address['plaats'];
    $address_country        = $address['land'];

endif;


//Get the page content
$page_form_content = $all_fields['cta_form_block'];

//Setup new empty fields
$form_title         = '';
$form_description   = '';

//Fill the content
if( $page_form_content ): //Enter group

    $form_title         = $page_form_content['title'];
    $form_description   = $page_form_content['description'];

else:

    $form_title         = $default_title;
    $form_description   = $default_description;

endif;

?>
<section class="pt-5">
    <div class="container">
        <div class="row">
            <div class="col-12 col-md-6">
                <h2 class=""><?php echo $form_title; ?></h2>
                <p class="description"><?php echo $form_description; ?></p>
                <div class="">
                    <a class="align-top" href="tel:<?php echo $contact_details_phone_link; ?>">
                        <svg xmlns="http://www.w3.org/2000/svg" class="svg-contacticon color--grey-alt mr-2" role="img" viewBox="0 0 25 32">
                            <path d="M12 0c-6.617 0-12 5.394-12 12.022 0 9.927 11.201 19.459 11.678 19.86 0.093 0.079 0.208 0.118 0.322 0.118s0.226-0.038 0.318-0.114c0.477-0.394 11.682-9.762 11.682-19.864 0-6.628-5.383-12.022-12-12.022zM12.002 30.838c-1.841-1.645-11.002-10.259-11.002-18.816 0-6.078 4.935-11.022 11-11.022s11 4.944 11 11.022c0 8.702-9.152 17.193-10.998 18.816zM12 6c-3.309 0-6 2.691-6 6s2.691 6 6 6 6-2.691 6-6-2.691-6-6-6zM12 17c-2.757 0-5-2.243-5-5s2.243-5 5-5 5 2.243 5 5-2.243 5-5 5z"></path>
                        </svg>
                        <?php echo $address_street . ',' . $address_postcode . ',' . $address_city; ?>
                    </a>
                </div>

                <div class="">
                    <a class="align-top" href="tel:<?php echo $contact_details_phone_link; ?>">
                        <svg xmlns="http://www.w3.org/2000/svg" class="svg-contacticon color--grey-alt mr-2" role="img" viewBox="0 0 20 32">
                            <path d="M1.5 32h16c0.827 0 1.5-0.673 1.5-1.5v-29c0-0.827-0.673-1.5-1.5-1.5h-16c-0.827 0-1.5 0.673-1.5 1.5v29c0 0.827 0.673 1.5 1.5 1.5zM1 1.5c0-0.276 0.224-0.5 0.5-0.5h16c0.276 0 0.5 0.224 0.5 0.5v29c0 0.276-0.224 0.5-0.5 0.5h-16c-0.276 0-0.5-0.224-0.5-0.5v-29zM2.5 27h14c0.276 0 0.5-0.224 0.5-0.5v-21c0-0.276-0.224-0.5-0.5-0.5h-14c-0.276 0-0.5 0.224-0.5 0.5v21c0 0.276 0.224 0.5 0.5 0.5zM3 6h13v20h-13v-20zM9 29c0 0.552 0.448 1 1 1s1-0.448 1-1c0-0.552-0.448-1-1-1s-1 0.448-1 1zM7.5 4h4c0.276 0 0.5-0.224 0.5-0.5s-0.224-0.5-0.5-0.5h-4c-0.276 0-0.5 0.224-0.5 0.5s0.224 0.5 0.5 0.5z"></path>
                        </svg>
                        <?php echo $contact_details_phone; ?>
                    </a>
                </div>

                <div class="">
                    <a class="align-top" href="mailto:<?php echo $contact_details_email; ?>">
                        <svg xmlns="http://www.w3.org/2000/svg" class="svg-contacticon color--grey-alt mr-2" role="img" viewBox="0 0 40 32">
                            <path d="M20.060 0.413c-0.58-0.533-1.539-0.533-2.119 0l-17.688 10.671c-0.15 0.091-0.242 0.253-0.242 0.428v18.922c0 0.863 0.706 1.566 1.574 1.566h34.83c0.868 0 1.574-0.703 1.574-1.566v-18.922c0-0.175-0.092-0.337-0.242-0.428l-17.687-10.671zM18.504 1.24c0.035-0.021 0.066-0.046 0.095-0.074 0.108-0.107 0.25-0.166 0.401-0.166s0.293 0.059 0.4 0.166c0.029 0.028 0.061 0.053 0.095 0.074l17.227 10.394-12.478 7.436c-0.237 0.142-0.315 0.448-0.174 0.686 0.094 0.157 0.26 0.244 0.43 0.244 0.087 0 0.175-0.022 0.255-0.070l12.245-7.29v17.757l-16.935-11.266c-0.538-0.429-1.594-0.429-2.096-0.025l-16.969 11.286v-17.752l12.244 7.29c0.080 0.048 0.169 0.070 0.256 0.070 0.17 0 0.336-0.087 0.43-0.244 0.141-0.237 0.063-0.544-0.174-0.686l-12.479-7.436 17.227-10.394zM36.090 31h-34.188l16.656-11.086c0.173-0.138 0.712-0.137 0.919 0.025l16.613 11.061zM6.5 13h25c0.276 0 0.5-0.224 0.5-0.5s-0.224-0.5-0.5-0.5h-25c-0.276 0-0.5 0.224-0.5 0.5s0.224 0.5 0.5 0.5z"></path>
                        </svg>
                        <?php echo $contact_details_email; ?>
                    </a>
                </div>
            </div>

            <div class="col-12 col-md-6 text-left">
                <?php echo do_shortcode('[contact-form-7 id="551" title="Contact form 1"]'); ?>
            </div>
        </div>
    </div>
</section>
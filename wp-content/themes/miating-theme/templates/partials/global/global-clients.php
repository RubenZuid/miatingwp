<?php

$all_fields = get_acf_fields(get_the_ID());

$clients    = $all_fields['clients'];

$clients_title     = '';
$clients_descr     = '';
$clients_services  = '';

if( $clients ):

    $clients_title     = $clients['title'];
    $clients_descr     = $clients['description'];
    $clients_services  = $clients['clients'];

endif;

?>

<section name="Klanten van Ruben Zuidervaart" class="">

    <div class="container">

        <?php
        //Do regular title template
        hm_get_template_part( '/templates/partials/global/small-parts/title-regular', [ 'color' => 'color--primary', 'content' => $clients_title ] ); ?>

        <div class="row justify-content-center">
            <div class="col-md-6">
                <p class="text-center">
                    <?php echo $clients_descr; ?>
                </p>
            </div>
        </div>

        <div class="row">
            <?php
            if($clients_services):
                $count = 1;
                foreach($clients_services as $service):

                    $service_title          = $service['title'];
                    $service_description    = $service['description'];
                    $service_logo           = $service['logo'];

                    ?>

                    <div class="col col-md-4 mb-4">
                        <div class="cardshadow p-4 h-100">
                            <h3 class="color--primary">
                                <?php echo $service_title; ?>
                            </h3>
                            <div class="clients__logo d-flex align-items-center justify-content-center">
                                <img class="clients__logo-img d-inline-flex lazy" data-src="<?php echo $service_logo['url']; ?>" />
                            </div>
                            <p>
                                <?php echo $service_description; ?>
                            </p>
                        </div>
                    </div>

                    <?php
                    if( $count % 3 == 0):
                        echo '</div><div class="row">';
                    endif;

                    $count++;
                endforeach;
            endif;
            ?>
        </div>

    </div>

</section>

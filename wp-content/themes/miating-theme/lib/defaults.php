<?php

/*===========================================
=            Load Theme Defaults            =
===========================================*/

/**
 *
 * Set default author box gravatar size
 *
 * @since 1.0.0
 *
 */

add_filter( 'genesis_author_box_gravatar_size', 'fiksogenesis_author_box_gravatar_size' );
function fiksogenesis_author_box_gravatar_size( $size ) {

	return '150';

}

/**
 * Add some image sizes
 *
 * @since 1.0.1
 */

add_image_size( 'reviewer-photo', 100, 100, true );
add_image_size( 'sponsor-logo', 300, 200, false );
add_image_size( 'organization-photo', 200, 200, true );

/**
 *
 * Limit the depth of primary and secondary navigations to 4 levels
 *
 * @since 1.0.0
 *
 */

add_filter( 'wp_nav_menu_args', 'fiksogenesis_nav_menu_args' );
function fiksogenesis_nav_menu_args( $args ) {

	if ( $args['theme_location'] == 'primary' || $args['theme_location'] == 'secondary' )
		$args['depth'] = 3;

	return $args;

}

/**
 *
 * Unregister parent page templates
 *
 * @since 1.0.0
 *
 */
add_filter( 'theme_page_templates', 'fiksogenesis_remove_page_templates' );
function fiksogenesis_remove_page_templates( $templates ) {

	unset( $templates['page_blog.php'] ); /* Default Blog Page Template */

	return $templates;

}

/**
 *
 * Reposition the default location of the primary and secondary navigations to be in the right header area
 *
 * @since 1.0.0
 *
 */
remove_action( 'genesis_after_header', 'genesis_do_nav' );
remove_action( 'genesis_after_header', 'genesis_do_subnav' );

add_action( 'genesis_header_right', 'genesis_do_nav' );
add_action( 'genesis_header_right', 'genesis_do_subnav' );


add_action( 'genesis_meta', __NAMESPACE__ . '\\remove_redundant_markup' );
/**
 * Remove the redundant .site-inner and .content-sidebar-wrap markup.
 *
 * @since 1.0.0
 */
function remove_redundant_markup() {
    // Remove .site-inner everywhere.
    add_filter( 'genesis_markup_site-inner', '__return_null' );
    // Remove .content-sidebar-wrap only when we're using full width content.
    if ( 'full-width-content' === genesis_site_layout() ) {
        add_filter( 'genesis_markup_content-sidebar-wrap', '__return_null' );
    }
}

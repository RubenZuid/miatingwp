<?php
/**
 * Genesis Framework.
 *
 * WARNING: This file is part of the core Genesis Framework. DO NOT edit this file under any circumstances.
 * Please do all modifications in the form of a child theme.
 *
 * @package Genesis\Templates
 * @author  StudioPress
 * @license GPL-2.0+
 * @link    https://my.studiopress.com/themes/genesis/
 */

add_action( 'genesis_before_content', 'content_markup_open', 15 );
add_action( 'genesis_after_content', 'content_markup_close', 15 );

function content_markup_open() {
    echo '<div class="container">';
}

function content_markup_close() {
    echo '</div>';
}

// This file handles pages, but only exists for the sake of child theme forward compatibility.
genesis();

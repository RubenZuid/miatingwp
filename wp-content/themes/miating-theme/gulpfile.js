/**
 *
 * Gulp task recipe to produce production ready files for a WordPress theme
 * @author Calvin Koepke
 * @version 1.0
 * @link https://twitter.com/cjkoepke
 *
 */

//* Load and define dependencies
var gulp = require( 'gulp' );
var plumber     = require('gulp-plumber');
var concat      = require('gulp-concat');
var sass        = require('gulp-sass');
var postcss     = require('gulp-postcss');
var browserSync = require('browser-sync').create();
var cssnano = require('gulp-cssnano');
var sourcemaps = require('gulp-sourcemaps');
var uglify = require( 'gulp-uglify' );
var pump = require('pump');
var rename = require( 'gulp-rename' );
var sort = require( 'gulp-sort' );
var criticalCss = require('gulp-penthouse');


//* Store paths
var PATHS = {
	js: './assets/js/',
	scss: './assets/scss/',
	build: {
		js: './build/js/',
		css: './build/css/'
	},
	critical_css: '.'
};

var scripts = [
	'assets/js/script.js'
];

var npmDependencies = {

};

var config = {
	browserSyncProxy: 'https://miating.test'
};


var taskLoader = [ 'scripts', 'scss', 'watch' ];

//* Gulp task to combine JS files, minify, and output to bundle.min.js
gulp.task( 'scripts', function(cb) {
	pump([
		gulp.src( PATHS.js + '**/*.js' ),
		sourcemaps.init(),
		uglify(),
		rename({ extname: '.min.js' }),
		sourcemaps.write('maps'),
		gulp.dest( PATHS.build.js )
	], cb);

	var stream = gulp.src(PATHS.js + '**/*.js')
	.pipe(browserSync.stream())
});


gulp.task('script-deps', function() {
	return new Promise(function (resolve) {
		for (var src in npmDependencies) {
			gulp.src(src)
				.pipe(gulp.dest('web/js/deps/' + npmDependencies[src]));
		}
		resolve();
	})
});

gulp.task('reload-js', function() {
	browserSync.reload();
});


gulp.task('scss', function() {
	return gulp.src("./assets/scss/style.scss")
		.pipe(plumber())
		.pipe(sass({
			errLogToConsole: true
		}))
		.pipe(postcss([
			require('tailwindcss')('./assets/tailwind.config.js'),
			require('autoprefixer')(),
		]))
		.pipe(cssnano({
			autoprefixer: {
				add: true
			},
		}))
		.pipe(gulp.dest('./'))
		.pipe(browserSync.stream())
});

gulp.task('watch', function() {
	browserSync.init({
		proxy: {
			target: config.browserSyncProxy
		},
		"rewriteRules": [
			{
				"match": "." + config.browserSyncProxy,
				"replace": ""
			}
		]
	});

	gulp.watch("assets/scss/**/*.scss", gulp.series('scss'));
	gulp.watch("assets/scss/**/*.scss", gulp.series('critical-css'));
	gulp.watch(scripts, gulp.series('scripts'));
	gulp.watch("**/*.phtml").on('change', browserSync.reload);
	gulp.watch("**/*.php").on('change', browserSync.reload);
});

gulp.task('critical-css', function () {
    return gulp.src('style.css')
        .pipe(criticalCss({
            out: 'critical-css.php', // output file name
            url: 'http://miating.test', // url from where we want penthouse to extract critical styles
            width: 1400, // max window width for critical media queries
            height: 900, // max window height for critical media queries
            userAgent: 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)' // pretend to be googlebot when grabbing critical page styles.
        }))
        .pipe(cssnano({
            autoprefixer: {
                add: true
            },
        }))
        .pipe(gulp.dest('./templates/')); // destination folder for the output file
});

//* Load tasks
gulp.task('build', gulp.series('scss', 'script-deps', 'scripts'));

gulp.task('default', gulp.series('build'));

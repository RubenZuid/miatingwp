<?php
/**
 * This file adds the after footer template to the bottom of the page
 *
 * Called from: genesis_footer @functions.php
 *
 * @package Miating
 * @author  Ruben Zuidervaart
 * @license GPL-2.0+
 */

$all_options    = get_acf_options();

$footer = $all_options['footer'];

$footer_credits = '';
$footer_socials = '';

$footer_socials_facebook    = '';
$footer_socials_quora       = '';
$footer_socials_linkedin    = '';

if( $footer ):
    $footer_credits = $footer['credits'];
    $footer_socials = $footer['socials'];

    if( $footer_socials ):

        $footer_socials_facebook    = $footer_socials['facebook_url'];
        $footer_socials_quora       = $footer_socials['quora_url'];
        $footer_socials_linkedin    = $footer_socials['linkedin_url'];

    endif;
endif;

?>

<div class="container-fluid background--modal">
    <div class="container">
        <div class="row py-2">
            <div class="col-6 text-left">
                <small><?php echo $footer_credits; ?></small>
            </div>
            <div class="col text-right flex flex-row justify-end">
                <?php if( $footer_socials_facebook ): ?>
                    <a class="color--white px-2" href="<?php echo $footer_socials_facebook; ?>">
                        <svg xmlns="http://www.w3.org/2000/svg" aria-hidden="true" focusable="false" data-prefix="fab" data-icon="facebook-f" class="svg-inline--fa fa-facebook-f fa-w-10" role="img" viewBox="0 0 320 512"><path fill="currentColor" d="M279.14 288l14.22-92.66h-88.91v-60.13c0-25.35 12.42-50.06 52.24-50.06h40.42V6.26S260.43 0 225.36 0c-73.22 0-121.08 44.38-121.08 124.72v70.62H22.89V288h81.39v224h100.17V288z"/></svg>
                    </a>
                <?php endif; ?>

                <?php if( $footer_socials_linkedin ): ?>
                    <a class="color--white px-2" href="<?php echo $footer_socials_linkedin; ?>">
                        <svg xmlns="http://www.w3.org/2000/svg" aria-hidden="true" focusable="false" data-prefix="fab" data-icon="quora" class="svg-inline--fa fa-quora fa-w-14" role="img" viewBox="0 0 448 512"><path fill="currentColor" d="M440.5 386.7h-29.3c-1.5 13.5-10.5 30.8-33 30.8-20.5 0-35.3-14.2-49.5-35.8 44.2-34.2 74.7-87.5 74.7-153C403.5 111.2 306.8 32 205 32 105.3 32 7.3 111.7 7.3 228.7c0 134.1 131.3 221.6 249 189C276 451.3 302 480 351.5 480c81.8 0 90.8-75.3 89-93.3zM297 329.2C277.5 300 253.3 277 205.5 277c-30.5 0-54.3 10-69 22.8l12.2 24.3c6.2-3 13-4 19.8-4 35.5 0 53.7 30.8 69.2 61.3-10 3-20.7 4.2-32.7 4.2-75 0-107.5-53-107.5-156.7C97.5 124.5 130 71 205 71c76.2 0 108.7 53.5 108.7 157.7.1 41.8-5.4 75.6-16.7 100.5z"/></svg>
                    </a>
                <?php endif; ?>

                <?php if( $footer_socials_quora ): ?>
                    <a class="color--white px-2" href="<?php echo $footer_socials_quora; ?>">
                        <svg xmlns="http://www.w3.org/2000/svg" aria-hidden="true" focusable="false" data-prefix="fab" data-icon="linkedin-in" class="svg-inline--fa fa-linkedin-in fa-w-14" role="img" viewBox="0 0 448 512"><path fill="currentColor" d="M100.28 448H7.4V148.9h92.88zM53.79 108.1C24.09 108.1 0 83.5 0 53.8a53.79 53.79 0 0 1 107.58 0c0 29.7-24.1 54.3-53.79 54.3zM447.9 448h-92.68V302.4c0-34.7-.7-79.2-48.29-79.2-48.29 0-55.69 37.7-55.69 76.7V448h-92.78V148.9h89.08v40.8h1.3c12.4-23.5 42.69-48.3 87.88-48.3 94 0 111.28 61.9 111.28 142.3V448z"/></svg>
                    </a>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>


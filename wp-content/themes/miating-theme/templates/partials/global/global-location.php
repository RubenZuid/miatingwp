<?php

$all_options    = get_acf_options();
$location   = $all_options['location'];

$location_title     = '';
$location_backgr    = '';

if( $location ):

    $location_title     = $location['title'];
    $location_content   = $location['content'];
    $location_backgr    = $location['background_image'];

endif;

?>

<section name="sponsors"
         class="mt-5 underglow relative"
         style="
                 background-image: url(<?php echo $location_backgr['url'];  ?>);
                 background-repeat: no-repeat;
                 background-position: center;
                 background-size: cover;
                 ">
    <div class="absolute color-overlay color-overlay--gradient">

    </div>

    <div class="relative py-6 flex flex-row justify-center items-center">

        <div class="w-10/12 md:w-6/12 bg-white border border-grey-200">
            <?php
            //Do regular title template
            hm_get_template_part( '/templates/partials/global/small-parts/title-regular', [ 'color' => 'color--primary', 'content' => $location_title ] ); ?>

            <div class="pb-6 px-4 px-sm-0">
                <div class="">
                    <div class="row justify-content-around align-items-center">
                        <div class="col-md-6 text-center">
                            <?= $location_content ?>
                            <a href="https://nl-nl.facebook.com/MX5meeting/" class="flex justify-center pt-4">
                                <svg xmlns="http://www.w3.org/2000/svg" aria-hidden="true"
                                     class="w-16" data-icon="facebook-square" data-prefix="fab" viewBox="0 0 448 512"><path fill="#3b5998" d="M400 32H48A48 48 0 000 80v352a48 48 0 0048 48h137.3V327.7h-63V256h63v-54.6c0-62.2 37-96.5 93.6-96.5 27.2 0 55.5 4.8 55.5 4.8v61h-31.2c-30.8 0-40.4 19.1-40.4 38.8V256h68.7l-11 71.7h-57.8V480H400a48 48 0 0048-48V80a48 48 0 00-48-48z"/></svg>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>xoFNBY9#uw@i


</section>

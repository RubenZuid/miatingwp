<?php
/**
 * Miating.
 *
 * This file adds the Page Contact page template
 *
 * Template Name: Page Contact
 * @package Miating
 * @author  Ruben Zuidervaart
 * @license GPL-2.0+
 */


// Forces full width content layout.
add_filter( 'genesis_site_layout', '__genesis_return_full_width_content' );

remove_action( 'genesis_loop', 'genesis_do_loop' );
//remove_action( 'genesis_before_content_sidebar_wrap', 'fikso_do_page_hero', 1 );

add_action( 'genesis_before_content', 'fikso_do_page', 1 );


//Setup the args for the loops

// Load page content
function fikso_do_page() {


    get_template_part( '/templates/partials/global/global-contact-information', 'Global contact information' );

    echo '<div class="py-4"></div>';

    ?>

<?php }

// Runs the Genesis loop.
genesis();
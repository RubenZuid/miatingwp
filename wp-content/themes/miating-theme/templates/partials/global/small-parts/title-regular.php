<?php

?>

<div class="row justify-content-center text-center pt-5 pb-4">
    <div class="col-12 col-xl-7 col-lg-10 col-md-10">
        <h3 class="sectiontitle h1 <?php echo $template_args['color']; ?> mb-0"><?php echo $template_args['content']; ?></h3>
    </div>
</div>

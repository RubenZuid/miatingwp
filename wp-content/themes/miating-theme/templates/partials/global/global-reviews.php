<?php

$all_fields = get_acf_fields(get_the_ID());

$reviews    = $all_fields['reviews'];

$reviews_title      = '';
$reviews_descr      = '';
$reviews_reviews    = '';

if( $reviews ):

    $reviews_title      = $reviews['title'];
    $reviews_descr      = $reviews['description'];
    $reviews_reviews    = $reviews['reviews'];

endif;

?>

<section name="Reviews over Ruben Zuidervaart" class="background--grey-light">

    <div class="container mt-5 pb-5">

        <?php
        //Do regular title template
        hm_get_template_part( '/templates/partials/global/small-parts/title-regular', [ 'color' => 'color--primary', 'content' => $reviews_title ] ); ?>

        <?php if($reviews_descr): ?>
            <div class="row justify-content-center">
                <div class="col-md-6">
                    <p class="text-center">
                        <?php echo $reviews_descr; ?>
                    </p>
                </div>
            </div>
        <?php endif; ?>

        <div class="row pt-5 align-items-center">
            <?php
            if($reviews_reviews):
                $count = 1;
                foreach($reviews_reviews as $review):

                    $review_name    = $review['person'];
                    $review_content = $review['content'];
                    $review_photo   = $review['photo'];
                    $review_company = $review['company'];

                    ?>

                    <div itemprop="review" itemscope="" itemtype="http://schema.org/Review" class="col-md-6 review">
                        <div class="relative cardshadow background--white p-4">
                            <img data-src="<?php echo $review_photo['sizes']['reviewer-photo']; ?>"
                                 class="wp-post-image review__photo absolute cardshadow lazy"
                                 alt="<?php echo $review_photo['alt']; ?>">
                            <blockquote itemprop="reviewBody" class="review__quote pt-5">
                                <div>
                                    <?php echo $review_content; ?>
                                </div>
                            </blockquote>
                            <h5 itemprop="author" class="review__author">
                                <?php echo $review_name; ?>
                            </h5>
                            <span class="review__company">
                            <?php echo $review_company; ?>
                        </span>
                        </div>
                    </div>

                    <?php
                    if( $count % 3 == 0):
                        echo '</div><div class="row">';
                    endif;

                    $count++;
                endforeach;
            endif;
            ?>
        </div>

    </div>

</section>

<?php

/*==================================================
=            Starter Theme Introduction            =
==================================================*/

/**
 *
 * About FiksoGenesis
 * --------------
 * FiksoGenesis is a Genesis Child-theme that has derived from the "Starter Theme" project by Calvin Koepke to create a starter theme for Genesis Framework developers that doesn't over-bloat
 * their starting base. It includes commonly used templates, codes, and styles, along with optional SCSS and Gulp tasking.
 *
 * Credits and Licensing
 * --------------
 * Starter was created by Calvin Koepke, and is under GPL 2.0+.

 * Find me on Twitter: @cjkoepke
 *
 * --------------
 * The theme was then further developed by Miating under the lead of Ruben Zuidervaart into a besproke child theme, continuing the GPL2.0+ licence.
 *
 */


/*============================================
=            Begin Functions File            =
============================================*/

/**
 *
 * Define Child Theme Constants
 *
 * @since 1.0.0
 *
 */
define( 'CHILD_THEME_NAME', 'Miating' );
define( 'CHILD_THEME_AUTHOR', 'Ruben Zuidervaart' );
define( 'CHILD_THEME_AUTHOR_URL', 'http://www.rubenzuidervaart.nl' );
define( 'CHILD_THEME_URL', 'http://www.rubenzuidervaart.nl' );
define( 'CHILD_THEME_VERSION', '1.0.15' );
define( 'TEXT_DOMAIN', 'miating' );

/**
 *
 * Start the engine
 *
 * @since 1.0.0
 *
 */
include_once( get_template_directory() . '/lib/init.php');

/**
 *
 * Load files in the /assets/ directory
 *
 * @since 1.0.0
 *
 */
add_action( 'wp_enqueue_scripts', 'fiksogenesis_load_assets', 6 );
function fiksogenesis_load_assets() {

    // Load OWL carousel2
    //wp_enqueue_script( 'owl-carousel', get_stylesheet_directory_uri() . '/build/js/vendor/owlcarousel2/owl.carousel.min.js', array( 'jquery' ), CHILD_THEME_VERSION, true );

    // Load jQuery Lazyload plugin
    //wp_enqueue_script( 'jquery.lazy', get_stylesheet_directory_uri() . '/build/js/vendor/jquery.lazy-master/jquery.lazy.min.js', array( 'jquery' ), CHILD_THEME_VERSION, true );
    //Lazy iFrame embedded into jquery.lazy
    //wp_enqueue_script( 'jquery.lazy.iframe', get_stylesheet_directory_uri() . '/build/js/vendor/jquery.lazy-master/plugins/jquery.lazy.iframe.min.min.js', array( 'jquery' ), CHILD_THEME_VERSION, true );

    //Load jQuery Paroller plugin
    //wp_enqueue_script( 'paroller.js', get_stylesheet_directory_uri() . '/build/js/vendor/paroller.js-master/dist/jquery.paroller.min.js', array( 'jquery' ), CHILD_THEME_VERSION, true );

    // Load multiple in one
    wp_enqueue_script( 'fiksogenesis-scripts', get_stylesheet_directory_uri() . '/build/js/scripts.min.js', array( 'jquery' ), CHILD_THEME_VERSION, true );

	// Load JS.
	wp_enqueue_script( 'fiksogenesis-global', get_stylesheet_directory_uri() . '/build/js/global.min.js', array( 'jquery' ), CHILD_THEME_VERSION, true );

	// Load default icons.
	//wp_enqueue_style( 'dashicons' );

	// Load responsive menu.
	/*$suffix = defined( SCRIPT_DEBUG ) && SCRIPT_DEBUG ? '' : '.min';
	wp_enqueue_script( 'fiksogenesis-responsive-menu', get_stylesheet_directory_uri() . '/build/js/responsive-menus' . $suffix . '.js', array( 'jquery', 'fiksogenesis-global' ), CHILD_THEME_VERSION, true );
	wp_localize_script(
		'fiksogenesis-responsive-menu',
		'genesis_responsive_menu',
	 	starter_get_responsive_menu_args()
	);*/

    //Wait with style.css till footer
    wp_dequeue_style(genesis_get_theme_handle());
    wp_deregister_script( 'jquery' );
    wp_register_script( 'jquery', includes_url( '/js/jquery/jquery.js' ), false, NULL, true );
    wp_enqueue_script( 'jquery' );


    //Now clean up some garbage
    wp_deregister_style( 'wp-block-library' ); //Gutenberg styles
    wp_deregister_script( 'wp-embed' ); //If you want to embed other Blog's posts

    wp_dequeue_style( 'superfish' );
    wp_dequeue_style( 'superfish-args' );
    wp_deregister_script( 'superfish' );
    wp_deregister_script( 'superfish-args' );

    //And finally fix plugin scripts and stylesheets, minify etc.
    wp_deregister_script( 'contact-form-7' );
    wp_deregister_script( 'jquery-form' );

    //wp_enqueue_script( 'contact-form-7-minified', get_stylesheet_directory_uri() . '/build/js/plugins/contact-forms-7/scripts.min.js', array( 'jquery' ), CHILD_THEME_VERSION, true );
}

add_action( 'wp_enqueue_scripts','child_dequeue_skip_links' );
function child_dequeue_skip_links() {
    wp_dequeue_script( 'skip-links' );
}

/**
 * Load the main styles in the footer!
 */
add_action( 'get_footer', 'footer_styles' );
function footer_styles() {
    wp_enqueue_style(
        genesis_get_theme_handle(),
        get_stylesheet_uri(),
        false,
        genesis_get_theme_version()
    );
}

//Bye Contact Form 7 styles and scripts
add_filter( 'wpcf7_load_js', '__return_false' );
add_filter( 'wpcf7_load_css', '__return_false' );
//remove_action( 'map_meta_cap', 'wpcf7_map_meta_cap', 10 );

//Bye bye Emoji's
remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
remove_action( 'wp_print_styles', 'print_emoji_styles' );

/**
 * Sort out the "above the fold" CSS
 */

//add_action( 'wp_head', 'fikso_above_fold_css', 5 );
function fikso_above_fold_css() { ?>

    <style type="text/css">
        <?php get_template_part('/templates/critical-css', 'Critical CSS' ); ?>
    </style>

    <link rel="preload" href="/wp-content/themes/miating/assets/fonts/Futura/web/futura-bold-webfont.woff" as="font">
    <link rel="preload" href="/wp-content/themes/miating/assets/fonts/Futura/web/futura-regular-webfont.woff" as="font">

    <?php
}

/**
 * Set the responsive menu arguments.
 *
 * @return array Array of menu arguments.
 *
 * @since 1.1.0
 */
function starter_get_responsive_menu_args() {

	$args = array(
		'mainMenu'         => __( 'Menu', TEXT_DOMAIN ),
		'menuIconClass'    => 'dashicons-before dashicons-menu',
		'subMenu'          => __( 'Menu', TEXT_DOMAIN ),
		'subMenuIconClass' => 'dashicons-before dashicons-arrow-down-alt2',
		'menuClasses'      => array(
			'combine' => array(
				'.nav-primary',
				'.nav-header',
				'.nav-secondary',
			),
			'others'  => array(
				'.nav-footer',
				'.nav-sidebar',
			),
		),
	);

	return $args;

}

/**
 *
 * Add theme supports
 *
 * @since 1.0.0
 *
 */
add_theme_support( 'genesis-responsive-viewport' ); /* Enable Viewport Meta Tag for Mobile Devices */
add_theme_support( 'html5',  array( 'search-form', 'comment-form', 'comment-list', 'gallery', 'caption' ) ); /* HTML5 */
add_theme_support( 'genesis-accessibility', array( 'skip-links', 'search-form', 'drop-down-menu', 'headings' ) ); /* Accessibility */
//add_theme_support( 'genesis-after-entry-widget-area' ); /* After Entry Widget Area */


/**
 * Add / Remove some menus
 */
unregister_sidebar( 'header-right' );

function register_additional_menus() {
    register_nav_menu( 'slidein-menu' ,__( 'Slide in Menu' ));
}
add_action( 'init', 'register_additional_menus' );


//Load slidein menu template
add_action( 'genesis_header_right', 'fikso_do_social_sticky', 0 );
function fikso_do_social_sticky() {
    get_template_part( '/templates/partials/global/global-social-sticky', 'Social sticky' );
}

//Load slidein menu template
add_action( 'genesis_before_header', 'fikso_do_slidenav', 2 );
function fikso_do_slidenav() {
    get_template_part( '/templates/partials/global/global-slidein-menu', 'Slide-in menu' );
}

/**
 * Allow for SVG images to also be uploaded and used
 *
 * @since 1.0.1
 *
 */
function cc_mime_types($mimes) {
    $mimes['svg'] = 'image/svg+xml';
    $mimes['webp'] = 'image/webp';
    return $mimes;
}
add_filter('upload_mimes', 'cc_mime_types');


/**
 *
 * Load extra functions
 *
 * @since 1.0.0
 * @uses /lib/functions.php
 *
 */
include_once( get_stylesheet_directory() . '/lib/functions.php' );


/**
 *
 * Apply custom body classes
 *
 * @since 1.0.0
 * @uses /lib/classes.php
 *
 */
include_once( get_stylesheet_directory() . '/lib/classes.php' );

/**
 *
 * Apply Starter Theme defaults (overrides default Genesis settings)
 *
 * @since 1.0.0
 * @uses /lib/defaults.php
 *
 */
include_once( get_stylesheet_directory() . '/lib/defaults.php' );

/**
 *
 * Apply Starter Theme default attributes
 *
 * @since 1.0.0
 * @uses /lib/attributes.php
 *
 */
include_once( get_stylesheet_directory() . '/lib/attributes.php' );

/**
 * Launch ACF Functionality
 */
include_once( get_stylesheet_directory() . '/lib/advanced-custom-fields/init.php' );

// Remove .wrap from menu-primary or other element by omitting them from the array below
add_theme_support( 'genesis-structural-wraps', array( 'header', 'menu-secondary', 'menu-primary' ) );


//Remove credentials
remove_action('genesis_footer', 'genesis_do_footer');

//Load modals
add_action( 'genesis_before_header', 'fikso_do_modal', 1 );
function fikso_do_modal() {
    get_template_part( '/templates/partials/global/global-modal-contact', 'Global contact modal' );
}

//Add footer map
//add_action( 'genesis_before_footer', 'fikso_footer_map' );
function fikso_footer_map() {
    get_template_part( '/templates/partials/footer/footer-before', 'footer map' );
}

//Add after footer
add_action( 'genesis_footer', 'fikso_do_after_footer' );
function fikso_do_after_footer() {
    get_template_part( '/templates/partials/footer/footer-after', 'footer after' );
}

//Load page header
add_action( 'genesis_before_content', 'fikso_do_page_header', 1 );
function fikso_do_page_header() {
    get_template_part( '/templates/partials/header', 'Header' );
}


// Load GTM scripts
//add_action( 'wp_head', 'fikso_GTM_head', 5 );
function fikso_GTM_head() { ?>

    <!-- Google Tag Manager -->

    <!-- End Google Tag Manager -->

    <?php
}

//add_action ( 'genesis_before', 'fikso_GTM_body', 1 );
function fikso_GTM_body() { ?>

    <!-- Google Tag Manager (noscript) -->

    <!-- End Google Tag Manager (noscript) -->

    <?php
}

function fix_svg_thumb_display() {
    echo '
    td.media-icon img[src$=".svg"], img[src$=".svg"].attachment-post-thumbnail { 
      width: 100% !important; 
      height: auto !important; 
    }
  ';
}
add_action('admin_head', 'fix_svg_thumb_display');
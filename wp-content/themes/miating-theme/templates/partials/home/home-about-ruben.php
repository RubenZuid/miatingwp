<?php

$all_fields = get_acf_fields( get_the_ID() );

$about_me   = $all_fields['about_miating'];

$about_me_title = '';
$about_me_repeater  = '';

if($about_me):

    $about_me_title = $about_me['title'];
    $about_me_repeater  = $about_me['image__text']; //Repeater

endif;

?>

<section name="about-miating">
    <div class="container">
        <?php
        //Do regular title template
        hm_get_template_part( '/templates/partials/global/small-parts/title-regular', [ 'color' => 'color--primary', 'content' => $about_me_title ] ); ?>

        <?php if( $about_me_repeater ):

            foreach( $about_me_repeater as $row ):

                $has_title  = $row['has_title'];
                if( $has_title ):
                    $title      = $row['title'];
                endif;

                $text_or_image  = $row['text_or_image'];

                $has_text   = '';
                $has_images = '';
                $content    = '';
                $images     = '';

                if( $row['text_or_image'] ):
                    $has_text   = true;
                    $content    = $row['content'];
                else:
                    $has_images = true;
                    $images     = $row['images'];
                endif;

                ?>

                <div class="row align-items-center pb-4">

                    <?php if( $images ): ?>

                        <div class="col-md-12">
                            <?php
                            if( $has_title ):
                                echo '<h3>' . $title . '</h3>';
                            endif;

                            echo $content;
                            ?>
                        </div>
                        <div class="col-md-12 text-center">
                            <div class="row justify-content-around">
                                <?php foreach( $images as $image ): ?>
                                    <div class="col group px-0">
                                        <div class="relative flex flex-row justify-center text-center">
                                            <img data-src="<?php echo $image['image']['sizes']['organization-photo'] ?>" class="rounded-circle lazy mx-auto" />
                                            <img src="<?php echo $image['hover_image']['sizes']['organization-photo'] ?>"
                                                 class="rounded-circle opacity-0 group-hover:opacity-100 transition-opacity duration-200 absolute inset-0 mx-auto" />
                                        </div>
                                    </div>
                                <?php endforeach; ?>
                            </div>
                        </div>

                    <?php else: ?>

                        <div class="col-md-12 text-center">
                            <?php
                            if( $has_title ):
                                echo '<h3>' . $title . '</h3>';
                            endif;

                            echo $content;
                            ?>
                        </div>

                    <?php endif; ?>

                </div>

                <?php
            endforeach;

        endif;
        ?>

    </div>
</section>

